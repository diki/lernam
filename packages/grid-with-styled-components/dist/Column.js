"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const styled_components_1 = __importDefault(require("styled-components"));
const devices_1 = __importDefault(require("./devices"));
exports.BASE_SCALE = 12;
const ColWrapper = styled_components_1.default.div `
  margin-right: ${props => props.gutter || 0}px;

  @media ${devices_1.default.lg} {
    width: calc(
      ${props => (100 * props.lg) / exports.BASE_SCALE}% -
        ${props => props.gutter || 0}px
    );
  }

  @media ${devices_1.default.md} {
    width: calc(
      ${props => (100 * props.md) / exports.BASE_SCALE}% -
        ${props => props.gutter || 0}px
    );
  }

  @media ${devices_1.default.sm} {
    width: calc(
      ${props => (100 * props.sm) / exports.BASE_SCALE}% -
        ${props => props.gutter || 0}px
    );
  }

  @media ${devices_1.default.xs} {
    width: ${props => (100 * props.xs) / exports.BASE_SCALE}%;
    margin-right: 0;
  }

  :last-child {
    margin-right: 0;
  }
`;
const Col = ({ children, gutter, lg, md, sm, xs, style }) => {
    let finalLg = lg || md || sm || xs;
    let finalMd = md || lg || sm || xs;
    let finalSm = sm || md || xs || lg;
    let finalXs = xs || sm || md || lg;
    if ((lg || md || sm || xs) === undefined) {
        console.warn("grid-for-style-components: No length prop is given to Col. Pass at least one of lg, xs, sm or md props to Col component");
    }
    return (react_1.default.createElement(ColWrapper, { lg: finalLg, md: finalMd, sm: finalSm, xs: finalXs, gutter: gutter, style: style }, children));
};
exports.default = Col;
