"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const styled_components_1 = __importDefault(require("styled-components"));
const RowWrapper = styled_components_1.default.div `
  width: 100%;
  display: flex;
  flex-wrap: ${props => (props.wrap ? "wrap" : "nowrap")};
  margin-right: -${props => props.gutter || 0}px;
`;
const BASE_SCALE = 12;
const Row = ({ children, gutter = 0, equal, stacked, doubling, wrap }) => {
    let cloned = children;
    if (children) {
        const addProps = {};
        const count = react_1.default.Children.count(children);
        // if equal prop is there evenly spread the space
        if (equal) {
            addProps.lg = BASE_SCALE / count;
            addProps.md = BASE_SCALE / count;
            addProps.sm = BASE_SCALE / count;
            addProps.xs = BASE_SCALE / count;
        }
        // if stacked show always stacked on small screens
        if (stacked) {
            addProps.xs = BASE_SCALE;
        }
        if (doubling) {
            addProps.xs = 16;
            addProps.sm = 8;
            if (count > 4 && count % 2 === 0) {
                addProps.md = (16 / count) * 2;
                addProps.lg = 16 / count;
            }
        }
        cloned = react_1.default.Children.map(children, child => {
            return react_1.default.cloneElement(child, Object.assign({ gutter }, addProps));
        });
    }
    return (react_1.default.createElement(RowWrapper, { gutter: gutter, wrap: wrap }, cloned));
};
exports.default = Row;
