/// <reference types="react" />
interface Props {
    gutter?: number;
    children?: JSX.Element[] | JSX.Element;
    equal?: boolean;
    stacked?: boolean;
    doubling?: boolean;
    wrap?: boolean;
}
declare const Row: ({ children, gutter, equal, stacked, doubling, wrap }: Props) => JSX.Element;
export default Row;
//# sourceMappingURL=Row.d.ts.map