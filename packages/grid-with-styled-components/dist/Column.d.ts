/// <reference types="react" />
export declare const BASE_SCALE = 12;
export interface Props {
    gutter?: number;
    lg?: number;
    md?: number;
    sm?: number;
    xs?: number;
    children?: JSX.Element | JSX.Element[];
    style?: Object;
}
declare const Col: ({ children, gutter, lg, md, sm, xs, style }: Props) => JSX.Element;
export default Col;
//# sourceMappingURL=Column.d.ts.map