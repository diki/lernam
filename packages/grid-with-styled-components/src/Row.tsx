import React, { ReactNode, ReactElement } from "react";
import styled from "styled-components";

import { Props as ColProps } from "./Column";

interface Props {
  gutter?: number;
  children?: JSX.Element[] | JSX.Element;
  equal?: boolean;
  stacked?: boolean;
  doubling?: boolean;
  wrap?: boolean;
}

const RowWrapper = styled.div<Props>`
  width: 100%;
  display: flex;
  flex-wrap: ${props => (props.wrap ? "wrap" : "nowrap")};
  margin-right: -${props => props.gutter || 0}px;
`;

const BASE_SCALE = 12;

const Row = ({
  children,
  gutter = 0,
  equal,
  stacked,
  doubling,
  wrap
}: Props) => {
  let cloned = children;

  if (children) {
    const addProps: ColProps = {};
    const count = React.Children.count(children);

    // if equal prop is there evenly spread the space
    if (equal) {
      addProps.lg = BASE_SCALE / count;
      addProps.md = BASE_SCALE / count;
      addProps.sm = BASE_SCALE / count;
      addProps.xs = BASE_SCALE / count;
    }

    // if stacked show always stacked on small screens
    if (stacked) {
      addProps.xs = BASE_SCALE;
    }

    if (doubling) {
      addProps.xs = 16;
      addProps.sm = 8;

      if (count > 4 && count % 2 === 0) {
        addProps.md = (16 / count) * 2;
        addProps.lg = 16 / count;
      }
    }

    cloned = React.Children.map(children, child => {
      return React.cloneElement(child, {
        gutter,
        ...addProps
      });
    });
  }

  return (
    <RowWrapper gutter={gutter} wrap={wrap}>
      {cloned}
    </RowWrapper>
  );
};

export default Row;
