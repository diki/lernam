import React from "react";
import styled from "styled-components";
import devices from "./devices";

export const BASE_SCALE = 12;

export interface Props {
  gutter?: number;
  lg?: number;
  md?: number;
  sm?: number;
  xs?: number;
  children?: JSX.Element | JSX.Element[];
  style?: Object;
}

const ColWrapper = styled.div<Props>`
  margin-right: ${props => props.gutter || 0}px;

  @media ${devices.lg} {
    width: calc(
      ${props => (100 * (props.lg as number)) / BASE_SCALE}% -
        ${props => props.gutter || 0}px
    );
  }

  @media ${devices.md} {
    width: calc(
      ${props => (100 * (props.md as number)) / BASE_SCALE}% -
        ${props => props.gutter || 0}px
    );
  }

  @media ${devices.sm} {
    width: calc(
      ${props => (100 * (props.sm as number)) / BASE_SCALE}% -
        ${props => props.gutter || 0}px
    );
  }

  @media ${devices.xs} {
    width: ${props => (100 * (props.xs as number)) / BASE_SCALE}%;
    margin-right: 0;
  }

  :last-child {
    margin-right: 0;
  }
`;

const Col = ({ children, gutter, lg, md, sm, xs, style }: Props) => {
  let finalLg = lg || md || sm || xs;
  let finalMd = md || lg || sm || xs;
  let finalSm = sm || md || xs || lg;
  let finalXs = xs || sm || md || lg;

  if ((lg || md || sm || xs) === undefined) {
    console.warn(
      "grid-for-style-components: No length prop is given to Col. Pass at least one of lg, xs, sm or md props to Col component"
    );
  }

  return (
    <ColWrapper
      lg={finalLg}
      md={finalMd}
      sm={finalSm}
      xs={finalXs}
      gutter={gutter}
      style={style}
    >
      {children}
    </ColWrapper>
  );
};

export default Col;
