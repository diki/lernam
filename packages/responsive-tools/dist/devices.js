"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const size = {
    xs: "325px",
    sm: "768px",
    md: "992px",
    lg: "1200px"
};
exports.default = {
    xs: `(min-width: ${size.xs}) and (max-width: ${size.sm})`,
    sm: `(min-width: ${size.sm}) and (max-width: ${size.md})`,
    md: `(min-width: ${size.md}) and (max-width: ${size.lg})`,
    lg: `(min-width: ${size.lg})`
};
