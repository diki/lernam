"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const styled_components_1 = __importDefault(require("styled-components"));
exports.Headline = styled_components_1.default.div `
  font-size: 6rem;
`;
exports.Subheadline = styled_components_1.default.div `
  font-size: 5rem;
`;
exports.F1 = styled_components_1.default.div `
  font-size: 3rem;
`;
exports.F2 = styled_components_1.default.div `
  font-size: 2.25rem;
`;
exports.F3 = styled_components_1.default.div `
  font-size: 1.5rem;
`;
exports.F4 = styled_components_1.default.div `
  font-size: 1.25rem;
`;
exports.F5 = styled_components_1.default.div `
  font-size: 1rem;
`;
exports.F6 = styled_components_1.default.div `
  font-size: 0.875rem;
`;
