import styled from "styled-components";

export const Headline = styled.div`
  font-size: 6rem;
`;

export const Subheadline = styled.div`
  font-size: 5rem;
`;

export const F1 = styled.div`
  font-size: 3rem;
`;

export const F2 = styled.div`
  font-size: 2.25rem;
`;

export const F3 = styled.div`
  font-size: 1.5rem;
`;

export const F4 = styled.div`
  font-size: 1.25rem;
`;

export const F5 = styled.div`
  font-size: 1rem;
`;

export const F6 = styled.div`
  font-size: 0.875rem;
`;
