import React from "react";
import styled from "styled-components";
import { devices } from "responsive-tools";
import Head from "next/head";

import NavBar from "../src/components/NavBar";
import Footer from "../src/components/Footer";
import PostPage from "../src/components/PostPage";

const client = require("contentful").createClient({
  space: "ix4jc2zini6y",
  accessToken:
    "8ac6bd2e370d19bede49bc705cd6ee874c4a53d453bdff0efb1164428fdc3138"
});

const Index = (props: any) => (
  <div>
    <NavBar background="white" height={80} xsHeight={80} withScroll={false} />
    <PostPage />
    <Footer />
  </div>
);

Index.getInitialProps = async () => {
  const heroProps = {
    header: "i am header",
    bigtext: "i am big text"
  };

  const bigHeroImage = "/static/ahmet_png2.png";

  return {
    heroProps,
    bigHeroImage
  };
};

export default Index;
