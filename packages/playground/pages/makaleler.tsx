import React from "react";
import styled from "styled-components";
import { devices } from "responsive-tools";
import Head from "next/head";

import NavBar from "../src/components/NavBar";
import Footer from "../src/components/Footer";
import Posts from "../src/components/Posts";

const client = require("contentful").createClient({
  space: "ix4jc2zini6y",
  accessToken:
    "8ac6bd2e370d19bede49bc705cd6ee874c4a53d453bdff0efb1164428fdc3138"
});

const Index = (props: any) => (
  <div>
    <NavBar
      background="white"
      height={80}
      xsHeight={80}
      withScroll={false}
    ></NavBar>
    <Posts items={props.items} />
    <Footer />
  </div>
);

Index.getInitialProps = async () => {
  const types = await client.getContentTypes();

  const posts = types.items.find((t: any) => t.name === "makaleler");
  const entries = await client.getEntries({
    content_type: posts.sys.id
  });

  return {
    items: entries.items.map(item => ({ ...item.fields, sid: item.sys.id }))
  };
};

export default Index;
