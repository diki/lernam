import React from "react";
import styled from "styled-components";
import { devices } from "responsive-tools";
import Head from "next/head";
import { useRouter } from "next/router";

import NavBar from "../../src/components/NavBar";
import Footer from "../../src/components/Footer";
import PostPage from "../../src/components/PostPage";

const client = require("contentful").createClient({
  space: "ix4jc2zini6y",
  accessToken:
    "8ac6bd2e370d19bede49bc705cd6ee874c4a53d453bdff0efb1164428fdc3138"
});

const postItems = [
  {
    id: "implant",
    image: "/static/implant.jpg",
    title: "Implant Tedavisi",
    intro: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elit sed vulputate mi sit amet mauris. Platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Eget dolor morbi non arcu risus quis varius quam quisque. Facilisis magna etiam tempor orci eu lobortis elementum nibh. Ipsum dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Velit euismod in pellentesque massa placerat. Pharetra vel turpis nunc eget lorem. Lacus viverra vitae congue eu consequat ac. Turpis egestas pretium aenean pharetra magna ac. Et ligula ullamcorper malesuada proin libero nunc consequat interdum varius. Sed elementum tempus egestas sed. Egestas sed sed risus pretium quam vulputate.
        <br/><br/>
      Urna duis convallis convallis tellus id interdum. Nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Et tortor consequat id porta nibh venenatis cras. In pellentesque massa placerat duis ultricies lacus. Tempus urna et pharetra pharetra massa. Eget sit amet tellus cras. Ac odio tempor orci dapibus ultrices in iaculis. Felis bibendum ut tristique et egestas. In nulla posuere sollicitudin aliquam ultrices sagittis. Ut placerat orci nulla pellentesque dignissim enim sit amet. Neque vitae tempus quam pellentesque nec. Ut etiam sit amet nisl.
      
      Et leo duis ut diam quam. Senectus et netus et malesuada fames ac turpis egestas. Eget magna fermentum iaculis eu. Lectus sit amet est placerat in. Nibh ipsum consequat nisl vel pretium lectus quam id leo. Diam maecenas ultricies mi eget mauris pharetra et. Malesuada proin libero nunc consequat. Porttitor massa id neque aliquam vestibulum. Viverra accumsan in nisl nisi scelerisque eu ultrices vitae. Volutpat commodo sed egestas egestas. Ultrices mi tempus imperdiet nulla malesuada pellentesque elit. Risus sed vulputate odio ut. Ipsum suspendisse ultrices gravida dictum fusce ut placerat orci nulla. Id leo in vitae turpis massa. Nunc consequat interdum varius sit amet mattis.
      
      Scelerisque viverra mauris in aliquam sem fringilla ut morbi. Neque laoreet suspendisse interdum consectetur. Eu feugiat pretium nibh ipsum consequat nisl vel pretium. Adipiscing enim eu turpis egestas pretium. Sed velit dignissim sodales ut eu sem integer. Libero justo laoreet sit amet. Elit pellentesque habitant morbi tristique senectus. Maecenas volutpat blandit aliquam etiam erat. Sodales neque sodales ut etiam sit amet nisl. Urna duis convallis convallis tellus id interdum. Etiam erat velit scelerisque in dictum non consectetur a erat. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Aliquet lectus proin nibh nisl condimentum. Bibendum arcu vitae elementum curabitur vitae nunc sed velit. Nisl purus in mollis nunc sed id semper risus in.
      
      Enim neque volutpat ac tincidunt vitae. Eget mi proin sed libero enim sed faucibus turpis in. Lectus magna fringilla urna porttitor. Posuere lorem ipsum dolor sit amet consectetur adipiscing elit. Adipiscing elit pellentesque habitant morbi tristique senectus et netus. Nisl condimentum id venenatis a condimentum vitae sapien. Vel turpis nunc eget lorem. Pulvinar pellentesque habitant morbi tristique senectus et netus et malesuada. Metus vulputate eu scelerisque felis imperdiet proin fermentum. Condimentum lacinia quis vel eros donec ac odio tempor orci. Diam vel quam elementum pulvinar. Sit amet nulla facilisi morbi tempus iaculis urna. Nunc lobortis mattis aliquam faucibus purus in massa. Quis auctor elit sed vulputate mi sit amet mauris commodo. Rhoncus est pellentesque elit ullamcorper dignissim cras tincidunt. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Aliquam id diam maecenas ultricies mi eget mauris pharetra. Volutpat odio facilisis mauris sit. Platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Lacus sed viverra tellus in hac habitasse platea dictumst.`
  },
  {
    id: "dis-protezi",
    image: "/static/protez.jpg",
    title: "Dis Protezi",
    intro:
      "Dişlere yapılan protezler harektli ve sabit olmak üzere genellikle ikiye ayrılır. Hareketli protezler takıp çıkarılabilen, sabit protezler is"
  },
  {
    id: "lamina",
    image: "/static/lamina.jpg",
    title: "Implant Tedavisi",
    intro:
      "İmplant İMPLANT UYGULAMALARI Diş implantları, diş eksikliklerinde hastalarımıza fonksiyon,estetik ve sağlığını tekrar geri kazanmaları"
  },
  {
    id: "ortadonti",
    image: "/static/ortadonti.jpg",
    title: "Dis Protezi",
    intro:
      "Dişlere yapılan protezler harektli ve sabit olmak üzere genellikle ikiye ayrılır. Hareketli protezler takıp çıkarılabilen, sabit protezler is"
  }
];

const Index = (props: any) => (
  <div>
    <NavBar background="white" height={80} xsHeight={80} withScroll={false} />
    <PostPage {...props} />
    <Footer />
  </div>
);

Index.getInitialProps = async function(context: any) {
  const { id, sid } = context.query;
  const entry = await client.getEntry(sid);
  return { post: entry.fields };
};

export default Index;
