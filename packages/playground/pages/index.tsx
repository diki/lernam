import React from "react";
import styled from "styled-components";
import { devices } from "responsive-tools";
import Head from "next/head";

import NavBar from "../src/components/NavBar";
import FullScreenBackground from "../src/components/FullScreenBackground";
import HeroText from "../src/components/HeroText";
import Services from "../src/components/Services";
import About from "../src/components/About";
import Contact from "../src/components/Contact";
import Footer from "../src/components/Footer";

const LinkWrapper = styled.a`
  padding: 0.5rem 2rem;
  flex-grow: 1;
  flex-shrink: 1;
  flex-basis: 0;
  text-align: center;
  border: 1px solid black;
  max-width: 200px;
  display: block;
`;

const Logo = styled.div`
  display: flex;
  height: 60px;
  @media ${devices.md}, ${devices.sm}, ${devices.xs} {
    align-self: flex-start;
  }
`;

const LogoWrapper = styled.img`
  max-height: 60px;
  align-self: flex-start;
  @media ${devices.xs} {
    max-height: 48px;
  }
`;

const client = require("contentful").createClient({
  space: "ix4jc2zini6y",
  accessToken:
    "8ac6bd2e370d19bede49bc705cd6ee874c4a53d453bdff0efb1164428fdc3138"
});

const Index = (props: any) => (
  <div>
    <NavBar
      links={[
        { text: "Ana Sayfa", href: "" },
        { text: "Hizmetlerimiz", href: "/#/hizmetler" },
        { text: "Hakkimizda", href: "/#/hakkimizda" },
        { text: "Iletisim", href: "/#/iletisim" }
      ]}
      background="white"
      linkComponent={LinkWrapper}
      height={80}
      xsHeight={80}
      withScroll
    ></NavBar>
    <FullScreenBackground imageURL={props.bigHeroImage} />
    <HeroText {...props.heroProps} />
    <Services />
    <About />
    <Contact />
    <Footer />
  </div>
);

Index.getInitialProps = async () => {
  const heroProps = {
    header: "i am header",
    bigtext: "i am big text"
  };

  const bigHeroImage = "/static/ahmet_png2.png";
  // const types = await client.getContentTypes();

  // const hero = types.items.find((t: any) => t.name === "main-hero");
  // const entries = await client.getEntries({
  //   content_type: hero.sys.id
  // });
  // const heroProps = entries.items[0].fields;

  // const assets = await client.getAssets();
  // // console.log("asset items", assets.items);
  // const asset = await client.getAsset("3r1g5PqZ8g4BVnrE7YEIPC");

  return {
    heroProps,
    bigHeroImage
  };
};
export default Index;
