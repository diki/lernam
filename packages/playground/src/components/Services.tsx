import React, { useRef, useEffect } from "react";
import styled from "styled-components";
import { Headline, Subheadline, F1, F2, F3 } from "responsive-tools";
import { Row, Col } from "grid-with-styled-components";

import Item from "./ServiceItem3";

import EstetikIcon from "./icons/Estetik";
import ProtezIcon from "./icons/Protez";

const Line = styled.div`
  width: 100%;
  padding: 2rem 0;
  background-image: url(/static/bg-light-about.png);
  background-size: cover;
  background-position: center center;
  background-color: #f5f5f5;
`;

const Wrapper = styled.div`
  display: flex;
  width: 1160px;
  max-width: 100%;
  margin: 0 auto;
  padding: 0;
  flex-direction: column;
`;

const Centered = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
`;

const Title = styled.div`
  position: relative;
  &:before {
    content: "";
    position: absolute;
    width: 120px;
    height: 2px;
    background-color: #6f51c7;
    top: 55px;
    right: 0;
    left: 0;
    border-radius: 50px;
  }
  &:after {
    content: "";
    position: absolute;
    width: 30px;
    height: 6px;
    background-color: #ff8700;
    top: 53px;
    left: 0px;
    right: 0;
    border-radius: 50px;
  }
`;

const serviceItems = [
  {
    img: "/estetik.jpg",
    title: "Dis Estetigi",
    text:
      "DİŞ BEYAZLATMA Dişlerin yapısal olarak kendine has bir rengi vardır. Yenilen yemekler, sigara, çay, kahve, kola, şarap, vişne vs gibi",
    icon: EstetikIcon
  },
  {
    img: "/protez.jpg",
    title: "Dis Protezi",
    text:
      "Dişlere yapılan protezler harektli ve sabit olmak üzere genellikle ikiye ayrılır. Hareketli protezler takıp çıkarılabilen, sabit protezler is",
    icon: ProtezIcon
  },
  {
    img: "/implant.jpg",
    title: "Implant",
    text:
      "İmplant İMPLANT UYGULAMALARI Diş implantları, diş eksikliklerinde hastalarımıza fonksiyon,estetik ve sağlığını tekrar geri kazanmaları",
    icon: ProtezIcon
  },
  {
    img: "/ortadonti.jpg",
    title: "Ortadonti",
    text:
      "ORTODONTİ Diş çapraşıklıkları çocukluk döneminde süt dişlerin yerini asıl dişlerin alması ile başlayan bir süre&cced",
    icon: ProtezIcon
  },
  {
    img: "/kanal_tedavisi.jpg",
    title: "Kanal tedavisi",
    text:
      "KANAL TEDAVİSİ Her dişin ortasından onun beslenmesini ve duyarlılığını sağlayan damar ve sinir paketi geçer. Bu damar – sinir paketine",
    icon: ProtezIcon
  },
  {
    img: "/lamina.jpg",
    title: "Lamina",
    text:
      "LAMİNA Porselen Lamina Nedir ? Laminalar özellikle ön grup dişlerde uygulanabilen çok ince porselenlerdir.Porselenin fiziksel özel",
    icon: ProtezIcon
  }
];

const Services = () => {
  return (
    <div id="hizmetlerimiz">
      <Line>
        <Centered>
          <F2 style={{ fontWeight: 300 }}>Hizmetlerimiz</F2>
        </Centered>
        <Wrapper>
          <Row wrap={true}>
            {serviceItems.map(item => (
              <Col lg={4} md={4} sm={6} xs={12}>
                <div style={{ padding: "0 1rem" }}>
                  <Item
                    img={item.img}
                    text={item.text}
                    title={item.title}
                    icon={item.icon}
                  />
                </div>
              </Col>
            ))}
          </Row>
        </Wrapper>
      </Line>
    </div>
  );
};

export default Services;
