import React from "react";
import styled from "styled-components";
import BookingButton from "./BookingButton";
import { devices } from "responsive-tools";

const HeroWrapper = styled.div`
  height: 100vh;
  width: 1160px;
  margin: 0 auto;
  display: flex;
  align-items: center;
  max-width: 100%;
`;

const Hero = styled.div`
  font-size: 4.5rem;
  width: 50%;
  height: 60%;
  color: rgb(250, 250, 250);
  @media ${devices.md}, ${devices.sm}, ${devices.xs} {
    width: 100%;
    padding: 0 1rem;
  }
`;

const BigFont = styled.h1`
  font-size: 5rem;
  margin: 0.5rem 0;
  @media ${devices.xs} {
    font-size: 4rem;
    line-height: 4.5rem;
  }
`;

const MediumFont = styled.p`
  font-size: 2.25rem;
  margin: 0.5rem 0;
`;

const SmallFont = styled.p`
  font-size: 1.25rem;
  margin: 0.5rem 0;
`;

const HeroText = (props: any) => {
  return (
    <HeroWrapper>
      <Hero>
        <MediumFont>{props.header}</MediumFont>
        <BigFont>{props.bigtext}</BigFont>
        <SmallFont>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco{" "}
        </SmallFont>
        <BookingButton />
      </Hero>
    </HeroWrapper>
  );
};

export default HeroText;
