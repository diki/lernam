import React, { useRef, useEffect } from "react";
import styled from "styled-components";
import { F4, F5 } from "responsive-tools";
import Icon from './icons/Estetik';

const Wrapper = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  border: 1px solid rgba(91, 140, 204, 0.5);
  border-radius: 0px;
  padding-bottom: 2rem;
  margin: 1rem 0;
  padding-top: 2rem;
  padding: 2rem;
`;

const ImgWrapper = styled.div`
  width: 100%;
  display: flex;
  height: 96px;
  overflow: hidden;
  justify-content: center;
`;

const FancyDiv = styled.div`
  width: 72px;
  display: flex;
  height: 100%;
  background-image: linear-gradient(
    to bottom,
    rgba(91, 140, 204, 0.9),
    rgba(0, 0, 0, 0.6)
  );
  
`;

const Img = styled.img`
  height: 100%;
  width: calc(100% - 72px);
`;

const TextWrapper = styled.div`
  width: 100%;
  padding: 0 1rem;
  text-align: center;
`;

interface Props {
  img: string;
  title: string;
  text: string;
}

const ServicesItem = (props: any) => {
  return (
    <Wrapper>
      <ImgWrapper>
        <props.icon width={128} height={128} fill={'#555'}/>
      </ImgWrapper>
      <TextWrapper>
        <F4
          style={{
            fontWeight: 600,
            textTransform: "uppercase",
            margin: "1.25rem 0",
            color: '#555'
          }}
        >
          {props.title}
        </F4>
        <F5 style={{ fontWeight: 400, color: 'rgba(91, 140, 204, 0.9)' }}>{props.text}</F5>
      </TextWrapper>
    </Wrapper>
  );
};

export default ServicesItem;
