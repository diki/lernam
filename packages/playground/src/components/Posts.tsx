import React, { useRef, useEffect } from "react";
import styled from "styled-components";
import { Headline, Subheadline, F1, F2, F3 } from "responsive-tools";
import { Row, Col } from "grid-with-styled-components";
import Link from "next/link";

import Item from "./PostItem";

const Line = styled.div`
  width: 100%;
  margin-top: 9rem;
  margin-bottom: 4rem;
`;

const Wrapper = styled.div`
  display: flex;
  width: 960px;
  max-width: 100%;
  margin: 0 auto;
  padding: 0;
  flex-direction: column;
`;

const Centered = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
`;

const postItems = [
  {
    id: "implant",
    image: "/static/implant.jpg",
    title: "Implant Tedavisi",
    content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elit sed vulputate mi sit amet mauris. Platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Eget dolor morbi non arcu risus quis varius quam quisque. Facilisis magna etiam tempor orci eu lobortis elementum nibh. Ipsum dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Velit euismod in pellentesque massa placerat. Pharetra vel turpis nunc eget lorem. Lacus viverra vitae congue eu consequat ac. Turpis egestas pretium aenean pharetra magna ac. Et ligula ullamcorper malesuada proin libero nunc consequat interdum varius. Sed elementum tempus egestas sed. Egestas sed sed risus pretium quam vulputate.
        <br/><br/>
      Urna duis convallis convallis tellus id interdum. Nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Et tortor consequat id porta nibh venenatis cras. In pellentesque massa placerat duis ultricies lacus. Tempus urna et pharetra pharetra massa. Eget sit amet tellus cras. Ac odio tempor orci dapibus ultrices in iaculis. Felis bibendum ut tristique et egestas. In nulla posuere sollicitudin aliquam ultrices sagittis. Ut placerat orci nulla pellentesque dignissim enim sit amet. Neque vitae tempus quam pellentesque nec. Ut etiam sit amet nisl.
      
      Et leo duis ut diam quam. Senectus et netus et malesuada fames ac turpis egestas. Eget magna fermentum iaculis eu. Lectus sit amet est placerat in. Nibh ipsum consequat nisl vel pretium lectus quam id leo. Diam maecenas ultricies mi eget mauris pharetra et. Malesuada proin libero nunc consequat. Porttitor massa id neque aliquam vestibulum. Viverra accumsan in nisl nisi scelerisque eu ultrices vitae. Volutpat commodo sed egestas egestas. Ultrices mi tempus imperdiet nulla malesuada pellentesque elit. Risus sed vulputate odio ut. Ipsum suspendisse ultrices gravida dictum fusce ut placerat orci nulla. Id leo in vitae turpis massa. Nunc consequat interdum varius sit amet mattis.
      
      Scelerisque viverra mauris in aliquam sem fringilla ut morbi. Neque laoreet suspendisse interdum consectetur. Eu feugiat pretium nibh ipsum consequat nisl vel pretium. Adipiscing enim eu turpis egestas pretium. Sed velit dignissim sodales ut eu sem integer. Libero justo laoreet sit amet. Elit pellentesque habitant morbi tristique senectus. Maecenas volutpat blandit aliquam etiam erat. Sodales neque sodales ut etiam sit amet nisl. Urna duis convallis convallis tellus id interdum. Etiam erat velit scelerisque in dictum non consectetur a erat. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Aliquet lectus proin nibh nisl condimentum. Bibendum arcu vitae elementum curabitur vitae nunc sed velit. Nisl purus in mollis nunc sed id semper risus in.
      
      Enim neque volutpat ac tincidunt vitae. Eget mi proin sed libero enim sed faucibus turpis in. Lectus magna fringilla urna porttitor. Posuere lorem ipsum dolor sit amet consectetur adipiscing elit. Adipiscing elit pellentesque habitant morbi tristique senectus et netus. Nisl condimentum id venenatis a condimentum vitae sapien. Vel turpis nunc eget lorem. Pulvinar pellentesque habitant morbi tristique senectus et netus et malesuada. Metus vulputate eu scelerisque felis imperdiet proin fermentum. Condimentum lacinia quis vel eros donec ac odio tempor orci. Diam vel quam elementum pulvinar. Sit amet nulla facilisi morbi tempus iaculis urna. Nunc lobortis mattis aliquam faucibus purus in massa. Quis auctor elit sed vulputate mi sit amet mauris commodo. Rhoncus est pellentesque elit ullamcorper dignissim cras tincidunt. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Aliquam id diam maecenas ultricies mi eget mauris pharetra. Volutpat odio facilisis mauris sit. Platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Lacus sed viverra tellus in hac habitasse platea dictumst.`
  },
  {
    id: "dis-protezi",
    image: "/static/protez.jpg",
    title: "Dis Protezi",
    content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elit sed vulputate mi sit amet mauris. Platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Eget dolor morbi non arcu risus quis varius quam quisque. Facilisis magna etiam tempor orci eu lobortis elementum nibh. Ipsum dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Velit euismod in pellentesque massa placerat. Pharetra vel turpis nunc eget lorem. Lacus viverra vitae congue eu consequat ac. Turpis egestas pretium aenean pharetra magna ac. Et ligula ullamcorper malesuada proin libero nunc consequat interdum varius. Sed elementum tempus egestas sed. Egestas sed sed risus pretium quam vulputate.
    <br/><br/>
  Urna duis convallis convallis tellus id interdum. Nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Et tortor consequat id porta nibh venenatis cras. In pellentesque massa placerat duis ultricies lacus. Tempus urna et pharetra pharetra massa. Eget sit amet tellus cras. Ac odio tempor orci dapibus ultrices in iaculis. Felis bibendum ut tristique et egestas. In nulla posuere sollicitudin aliquam ultrices sagittis. Ut placerat orci nulla pellentesque dignissim enim sit amet. Neque vitae tempus quam pellentesque nec. Ut etiam sit amet nisl.
  
  Et leo duis ut diam quam. Senectus et netus et malesuada fames ac turpis egestas. Eget magna fermentum iaculis eu. Lectus sit amet est placerat in. Nibh ipsum consequat nisl vel pretium lectus quam id leo. Diam maecenas ultricies mi eget mauris pharetra et. Malesuada proin libero nunc consequat. Porttitor massa id neque aliquam vestibulum. Viverra accumsan in nisl nisi scelerisque eu ultrices vitae. Volutpat commodo sed egestas egestas. Ultrices mi tempus imperdiet nulla malesuada pellentesque elit. Risus sed vulputate odio ut. Ipsum suspendisse ultrices gravida dictum fusce ut placerat orci nulla. Id leo in vitae turpis massa. Nunc consequat interdum varius sit amet mattis.
  
  Scelerisque viverra mauris in aliquam sem fringilla ut morbi. Neque laoreet suspendisse interdum consectetur. Eu feugiat pretium nibh ipsum consequat nisl vel pretium. Adipiscing enim eu turpis egestas pretium. Sed velit dignissim sodales ut eu sem integer. Libero justo laoreet sit amet. Elit pellentesque habitant morbi tristique senectus. Maecenas volutpat blandit aliquam etiam erat. Sodales neque sodales ut etiam sit amet nisl. Urna duis convallis convallis tellus id interdum. Etiam erat velit scelerisque in dictum non consectetur a erat. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus. Aliquet lectus proin nibh nisl condimentum. Bibendum arcu vitae elementum curabitur vitae nunc sed velit. Nisl purus in mollis nunc sed id semper risus in.
  
  Enim neque volutpat ac tincidunt vitae. Eget mi proin sed libero enim sed faucibus turpis in. Lectus magna fringilla urna porttitor. Posuere lorem ipsum dolor sit amet consectetur adipiscing elit. Adipiscing elit pellentesque habitant morbi tristique senectus et netus. Nisl condimentum id venenatis a condimentum vitae sapien. Vel turpis nunc eget lorem. Pulvinar pellentesque habitant morbi tristique senectus et netus et malesuada. Metus vulputate eu scelerisque felis imperdiet proin fermentum. Condimentum lacinia quis vel eros donec ac odio tempor orci. Diam vel quam elementum pulvinar. Sit amet nulla facilisi morbi tempus iaculis urna. Nunc lobortis mattis aliquam faucibus purus in massa. Quis auctor elit sed vulputate mi sit amet mauris commodo. Rhoncus est pellentesque elit ullamcorper dignissim cras tincidunt. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Aliquam id diam maecenas ultricies mi eget mauris pharetra. Volutpat odio facilisis mauris sit. Platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Lacus sed viverra tellus in hac habitasse platea dictumst.`
  },
  {
    id: "lamina",
    image: "/static/lamina.jpg",
    title: "Implant Tedavisi",
    content:
      "İmplant İMPLANT UYGULAMALARI Diş implantları, diş eksikliklerinde hastalarımıza fonksiyon,estetik ve sağlığını tekrar geri kazanmaları"
  },
  {
    id: "ortadonti",
    image: "/static/ortadonti.jpg",
    title: "Dis Protezi",
    content:
      "Dişlere yapılan protezler harektli ve sabit olmak üzere genellikle ikiye ayrılır. Hareketli protezler takıp çıkarılabilen, sabit protezler is"
  }
];

const Posts = (props: any) => {
  return (
    <Line>
      <Wrapper>
        {/* <F2 style={{ fontWeight: 300, marginBottom: "1rem" }}>Makaleler</F2> */}
        <Row wrap={true}>
          {props.items.map((item: any) => (
            <Col lg={12} md={12} sm={12} xs={12}>
              <Link
                href={`/makaleler/[id]?sid=${item.sid}`}
                as={`/makaleler/${item.id}?sid=${item.sid}`}
              >
                <a
                  style={{
                    marginBottom: "2rem",
                    display: "block",
                    cursor: "pointer",
                    textDecoration: "none",
                    color: "black"
                  }}
                >
                  <Item {...item} />
                </a>
              </Link>
            </Col>
          ))}
        </Row>
      </Wrapper>
    </Line>
  );
};

export default Posts;
