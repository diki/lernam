import React, { useState, FunctionComponent } from "react";
import styled from "styled-components";

const LinkWrapper = styled.a`
  padding: 0.5rem 2rem;
  flex-grow: 1;
  flex-shrink: 1;
  flex-basis: 0;
  width: 160px;
  display: flex;
  justify-content: center;
  align-items: center;
  text-decoration: none;
  color: white;

  &.active {
    border-bottom: 2px solid white;
  }
`;

class NavLink extends React.Component<any> {
  render() {
    return (
      <LinkWrapper className="active" href={this.props.href}>{this.props.text}</LinkWrapper>
    );
  }
}

export default NavLink;
