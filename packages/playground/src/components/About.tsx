import React, { useRef, useEffect } from "react";
import styled from "styled-components";
import { F4, F5 } from "responsive-tools";
import { Row, Col } from "grid-with-styled-components";
import devices from "../devices";

const Line = styled.div`
  width: 100%;
  padding: 0;
  background: radial-gradient(ellipse at center, #585858 0%, #232323 100%);
  color: white;
  @media ${devices.md}, ${devices.sm}, ${devices.xs} {
    padding: 0 1rem;
  }
`;

const Wrapper = styled.div`
  display: flex;
  width: 1160px;
  max-width: 100%;
  margin: 0 auto;
  padding: 2rem 0;
  flex-direction: column;
`;

const Centered = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  padding-bottom: 2rem;
`;

const Bg = styled.div`
  background-image: url("/static/ahmet_png2.png");
  background-repeat: no-repeat;
  background-size: contain;
  height: 80vh;
`;

const TextWrapper = styled.div``;

const About = () => {
  return (
    <Line id="hakkimizda">
      <Wrapper>
        <Row gutter={16} wrap={true}>
          <Col lg={6} md={6} sm={12} xs={12}>
            <Bg />
          </Col>
          <Col
            lg={6}
            md={6}
            sm={12}
            xs={12}
            style={{ display: "flex", alignItems: "center" }}
          >
            <TextWrapper>
              <F4 style={{ fontWeight: 200 }}>
                <div style={{ padding: "4px 0" }}>
                  Diş Hekimi Ahmet Dikici 2017 yılında Erciyes Üniversitesi Diş
                  Hekimliği Fakültesin'den mezun olmuştur.
                </div>{" "}
                <div style={{ padding: "4px 0" }}>
                  2017 itibari ile Dentova İmplant ve Estetik Akademi Ekibine
                  katılmış,ortaklarından olmuştur.
                </div>{" "}
                <div style={{ padding: "4px 0" }}>
                  Çalışmalrında daha çok estetik kozmetik diş hekimliği,gülüş
                  tasarımı,estetik dolgular,zirkonyum kaplama,kompozit lamina
                  veneer ,porselen lamina veneer ve implantoloji üzerin'de
                  yoğunlaşmıştır. Mesleki yenilikleri ve teknolojiyi yakından
                  takip eden Ahmet DİKİCİ TDB ve EDAD üyesidir. 2018 yılın'da
                  Faktörüm dergisin'de "hemofili hastalarının diş tedavileri ve
                  ağız sağlığı" konusunda yazısı yayımlanmıştır.
                </div>{" "}
                <div style={{ padding: "4px 0" }}>
                  Seyahat etmeyi,yeni kültürler tanımayı ve dünyayı gezmeyi çok
                  seven Dt. Ahmet DİKCİ çektiği fotoğrafları sosyal medya hesabı
                  üzerinden hastaları ve takipçileri ile paylaşmaktadır. Ahmet
                  DİKİCİ evlidir ve hali hazırda Dentova İmplant ve Estetik
                  Akademi'de hastalarına hizmet vermektedir.
                </div>
              </F4>
            </TextWrapper>
          </Col>
        </Row>
      </Wrapper>
    </Line>
  );
};

export default About;
