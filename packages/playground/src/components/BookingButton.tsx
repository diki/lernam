import React, { useRef, useEffect } from "react";
import styled from "styled-components";

const BigButton = styled.a`
  font-size: 1.25rem;
  background: rgb(9, 172, 58);
  text-transform: uppercase;
  padding: 0.75rem 3rem;
  margin-top: 1.5rem;
  display: inline-block;
  box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  border-radius: 8px;
`;

const BookingButton = () => {
  const buttonEl = useRef(null);

  useEffect(() => {
    // const pop1 = (window as any).typeformEmbed.makePopup(
    //   "https://diki.typeform.com/to/ddQvHk",
    //   {
    //     mode: "drawer_right",
    //     hideHeaders: true,
    //     hideFooter: true,
    //     autoClose: true
    //   }
    // );
    // (buttonEl.current as unknown as HTMLElement).addEventListener("click", () => {
    //   pop1.open();
    // });
  });

  return <BigButton ref={buttonEl}>Randevu Al</BigButton>;
};

export default BookingButton;
