import React, { useState, FunctionComponent } from "react";
import styled from "styled-components";
import Link from "next/link";

import devices from "../devices";
import PhoneIcon from "./icons/Phone";

interface Props {
  height?: number;
  xsHeight?: number;
  links?: Array<{ text: string; href: string }>;
  background?: string;
  linkComponent?: React.ComponentClass<any> | FunctionComponent<any>;
  withScroll?: boolean;
}

const BarWrapper = styled.nav<Partial<Props>>`
  width: 100%;
  display: flex;
  height: ${props => (props.height ? props.height + "px" : "auto")};
  flex-direction: row;
  transition: height 0.22s;
  flex-wrap: wrap;
  background: rgba(0, 0, 0, 0.65);
  position: fixed;
  top: 0;
  box-shadow: 0px 6px 20px rgba(0, 0, 0, 0.16);

  @media ${devices.xs}, ${devices.sm}, ${devices.md} {
    height: ${props => (props.xsHeight ? props.xsHeight + "px" : "auto")};
    overflow: hidden;
    padding: 1rem;
  }
`;

const LinksWrapper = styled.nav`
  display: flex;
  margin-left: auto;
  order: 2;
  @media ${devices.xs}, ${devices.sm}, ${devices.md} {
    order: 3;
    width: 100%;
    flex-direction: column;
    margin-top: 4px;
    align-items: center;
  }
`;

const LinkWrapper = styled.a`
  padding: 0.5rem 1rem;
  flex-grow: 1;
  flex-shrink: 1;
  flex-basis: 0;
  width: 144px;
  display: flex;
  justify-content: center;
  align-items: center;
  text-decoration: none;
  color: white;
  flex-direction: column;

  .border {
    width: 0;
    height: 2px;
    background: white;
    transition: all 0.32s ease;
    margin-top: 0.5rem;
  }

  &.active {
    .border {
      width: 100%;
    }
  }

  @media ${devices.xs}, ${devices.sm}, ${devices.md} {
    padding: 1rem;
  }
`;

const TopInfoBar = styled.div<{ height: number }>`
  display: flex;
  min-height: ${props => (props.height ? props.height + "px" : "auto")};
  width: 1160px;
  max-width: 100%;
  align-items: center;
  @media ${devices.xs}, ${devices.sm}, ${devices.md} {
    flex-wrap: wrap;
  }
`;

const QuickContactWrapper = styled.div`
  display: flex;
  margin-left: auto;
  align-items: flex-end;
  padding-top: 10px;
  order: 3;
  @media ${devices.md}, ${devices.sm} {
    order: 2;
    margin-left: auto;
    padding-top: 0;
    margin-right: 4rem;
    align-items: flex-start;
    display: block;
  }

  @media ${devices.xs} {
    display: none;
  }
`;

const ContactWrapper = styled.div`
  display: flex;
  color: white;
`;

const IconWrapper = styled.div`
  width: 48px;
  @media ${devices.xs} {
    width: 32px;
  }
`;

const IconTextWrapper = styled.div`
  font-size: 1.25rem;
  font-weight: 300;
  margin-left: 0.5rem;
  @media ${devices.xs} {
    font-size: 1rem;
  }
`;

const MenuIcon = styled.div<{ isActive: boolean }>`
  width: 24px;
  display: none;

  @media ${devices.xs}, ${devices.sm}, ${devices.md} {
    display: block;
    position: absolute;
    right: 1rem;
    top: 1rem;
  }

  &:after,
  &:before,
  & div {
    background-color: #fff;
    border-radius: 3px;
    content: "";
    display: block;
    height: 2px;
    margin: 7px 0;
    transition: all 0.2s ease-in-out;
  }

  &:before {
    transform: ${props =>
      props.isActive ? "translateY(9px) rotate(135deg)" : "initial"};
  }

  &:after {
    transform: ${props =>
      props.isActive ? "translateY(-9px) rotate(-135deg)" : "initial"};
  }

  & div {
    transform: ${props => (props.isActive ? "scale(0)" : "initial")};
  }
`;

const Logo = styled.div`
  display: flex;
  height: 60px;
  width: 169px;
  @media ${devices.md}, ${devices.sm}, ${devices.xs} {
    align-self: flex-start;
  }
`;

const LogoWrapper = styled.img`
  max-height: 60px;
  align-self: flex-start;
  @media ${devices.xs} {
    max-height: 48px;
  }
`;

class NavBar extends React.Component<Props, any> {
  handleScroll: (e: any) => void;
  constructor(props: Props) {
    super(props);
    const sections = ["hizmetlerimiz", "hakkimizda", "iletisim"];

    this.state = {
      activeEl: "",
      isToggled: false,
      xsHeight: props.xsHeight
    };

    this.handleScroll = () => {
      if (window.scrollY < 300) {
        this.setState({
          activeEl: ""
        });
        return;
      }
      sections.forEach(id => {
        const el = document.getElementById(id);
        if (el === null) {
          return;
        }
        const rect = (el as HTMLElement).getBoundingClientRect();
        if (rect.top < 80 && rect.top > 0 && this.state.activeEl !== id) {
          this.setState({
            activeEl: id
          });
        }
      });
    };
  }

  componentDidMount() {
    // const sections = ["services", "about", "contact"];

    // const sectionRect = sections.map(id => {
    //   const el = document.getElementById(id);
    //   const rect = (el as HTMLElement).getBoundingClientRect();

    //   return rect;
    // });
    let activeEl = "";
    if (window.location.pathname.indexOf("makaleler") >= 0) {
      activeEl = "makaleler";
    }

    this.setState({
      activeEl
    });

    if (this.props.withScroll) {
      window.addEventListener("scroll", this.handleScroll);
    }
  }

  render() {
    const { height = 72, background, xsHeight = 76 } = this.props;
    return (
      <div>
        <BarWrapper
          height={height}
          xsHeight={this.state.xsHeight}
          background={background}
        >
          <TopInfoBar height={height}>
            <Logo>
              <LogoWrapper src="/static/logo.png" />
            </Logo>
            <LinksWrapper>
              <LinkWrapper
                className={this.state.activeEl === "" ? "active" : ""}
                href="/"
              >
                Ana Sayfa
                <div className="border"></div>
              </LinkWrapper>

              <LinkWrapper
                className={
                  this.state.activeEl === "hizmetlerimiz" ? "active" : ""
                }
                href="/#hizmetlerimiz"
                onClick={() => this.setState({ activeEl: "hizmetlerimiz" })}
              >
                Hizmetlerimiz
                <div className="border"></div>
              </LinkWrapper>

              <LinkWrapper
                className={this.state.activeEl === "hakkimizda" ? "active" : ""}
                href="/#hakkimizda"
                onClick={() => this.setState({ activeEl: "hakkimizda" })}
              >
                Hakkımızda
                <div className="border"></div>
              </LinkWrapper>

              <LinkWrapper
                className={this.state.activeEl === "iletisim" ? "active" : ""}
                href="/#iletisim"
                onClick={() => this.setState({ activeEl: "iletisim" })}
              >
                İletişim
                <div className="border"></div>
              </LinkWrapper>

              <Link href="/makaleler">
                <LinkWrapper
                  className={
                    this.state.activeEl === "makaleler" ? "active" : ""
                  }
                  href="/#/iletisim"
                >
                  Makaleler
                  <div className="border"></div>
                </LinkWrapper>
              </Link>
            </LinksWrapper>
            <QuickContactWrapper>
              <ContactWrapper>
                <IconWrapper>
                  <PhoneIcon fill="rgb(82, 169, 221)" />
                </IconWrapper>
                <IconTextWrapper>
                  <div style={{ fontSize: "1rem" }}>Telefon</div>0332 324 24 74
                </IconTextWrapper>
              </ContactWrapper>
            </QuickContactWrapper>
          </TopInfoBar>

          <MenuIcon
            isActive={this.state.isToggled}
            onClick={() => {
              this.setState({
                isToggled: !this.state.isToggled
              });
              if (this.state.xsHeight > xsHeight) {
                this.setState({
                  xsHeight
                });
              } else {
                this.setState({
                  xsHeight: xsHeight * 3.2
                });
              }
            }}
          >
            <div></div>
          </MenuIcon>
        </BarWrapper>
      </div>
    );
  }
}
// const NavBar = ({
//   height = 80,
//   logo,
//   links = [],
//   background,
//   xsHeight = 76
// }: Props) => {
//   const [xsHeightState, setHeight] = useState(xsHeight);
//   const [isToggled, setToggled] = useState(false);

//   return (
//     <div>
//       <BarWrapper
//         height={height}
//         xsHeight={xsHeightState}
//         background={background}
//       >
//         <TopInfoBar height={height}>
//           {logo}
//           <LinksWrapper>
//             {links.map(link => (
//               <LinkWrapper className="active" {...link}>{link.text}</LinkWrapper>
//             ))}
//           </LinksWrapper>
//           <QuickContactWrapper>
//             <ContactWrapper>
//               <IconWrapper>
//                 <PhoneIcon fill="rgb(82, 169, 221)" />
//               </IconWrapper>
//               <IconTextWrapper>
//                 <div style={{ fontSize: '1rem' }}>Telefon</div>0332 324 24 74
//               </IconTextWrapper>
//             </ContactWrapper>
//           </QuickContactWrapper>
//         </TopInfoBar>

//         <MenuIcon
//           isActive={isToggled}
//           onClick={() => {
//             setToggled(!isToggled);
//             if (xsHeightState > xsHeight) {
//               setHeight(xsHeight);
//             } else {
//               setHeight(xsHeightState * 4);
//             }
//           }}
//         >
//           <div></div>
//         </MenuIcon>
//       </BarWrapper>
//     </div>
//   );
// };

export default NavBar;
