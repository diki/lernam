import React, { useRef, useEffect } from "react";
import styled from "styled-components";
import { F4, F5 } from "responsive-tools";
import { documentToHtmlString } from "@contentful/rich-text-html-renderer";
import Icon from "./ToothIcon";

const Wrapper = styled.div`
  display: flex;
  width: 100%;
  flex-direction: row;
  border: 1px solid white;
  border-radius: 10px;
`;

const ImgWrapper = styled.div`
  display: flex;
  height: 176px;
  min-width: 200px;
  max-width: 200px;
`;

const FancyDiv = styled.div`
  width: 72px;
  display: flex;
  height: 100%;
  background-image: linear-gradient(
    to bottom,
    rgba(91, 140, 204, 0.9),
    rgba(0, 0, 0, 0.6)
  );
`;

const Img = styled.img`
  width: 100%;
`;

const TextWrapper = styled.div`
  width: 100%;
  padding: 0 1rem;
  white-space: normal;
  overflow: hidden;
  text-overflow: ellipsis;
`;

interface Props {
  image: any;
  title: string;
  content: any;
  intro: string;
  sid: string;
}

const PostItem = (props: Props) => {
  return (
    <Wrapper>
      <ImgWrapper>
        <Img src={props.image.fields.file.url} />
      </ImgWrapper>
      <TextWrapper>
        <F4
          style={{
            fontWeight: 500,
            textTransform: "uppercase",
            marginBottom: "1rem"
          }}
        >
          {props.title}
        </F4>
        <F5 style={{ fontWeight: 300 }}>{props.intro}...</F5>
      </TextWrapper>
    </Wrapper>
  );
};

export default PostItem;
