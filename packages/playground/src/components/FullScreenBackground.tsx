import React from "react";
import styled from "styled-components";
import { devices } from "responsive-tools";

const Bg = styled.div<any>`
  background-image: url("${props => props.imageURL}");
  height: 100%;
  width: 100%;
  background-position-x: 90%;
  background-position-y: 6rem;
  background-repeat: no-repeat;
  background-size: contain;
  background-attachment: fixed;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  z-index: -1;

  &:before {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    height: 100vh;
    background-image: linear-gradient(
      to right,
      rgba(91, 140, 204, 0.9),
      rgba(158, 199, 200, 0.6)
    );
  }

  // @media ${devices.xs} {
  //   background-position-y: 4rem;
  //   background-position-x: 100%;
  // }
`;

const FullScreenBackground = (props: any) => {
  return <Bg imageURL={props.imageURL} />;
};

export default FullScreenBackground;
