import React, { useRef, useEffect } from "react";
import styled from "styled-components";
import { F4, F5, F6 } from "responsive-tools";
import Icon from "./icons/Estetik";

const Wrapper = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  background: white;
  padding: 2rem;
  align-items: center;
  margin: 1rem 0;
  // border-radius: 12px;
  box-shadow: 0 0 15px 0 rgba(46, 61, 73, 0.03);
  border-top-left-radius: 50px;
  border-bottom-right-radius: 50px;
  border-top-right-radius: 3px;
  border-bottom-left-radius: 3px;
  transition: all 0.32s ease;
  &:hover {
    * {
      transition: all 0.32s ease;
    }
    background-image: linear-gradient(
      rgb(52, 184, 192) 0%,
      rgb(111, 81, 199) 100%
    );
    .icon {
      fill: white !important;
    }
    .smalltext {
      color: white !important;
    }
    .bigtext {
      color: white !important;
    }
    .imgwrap {
      border-color: white !important;
    }
    .morebutton {
      border-color: white;
    }
  }
`;

const ImgWrapper = styled.div`
  width: 100%;
  display: flex;
  height: 128px;
  overflow: hidden;
  justify-content: center;
`;

const FancyDiv = styled.div`
  width: 72px;
  display: flex;
  height: 100%;
  background-image: linear-gradient(
    to bottom,
    rgba(91, 140, 204, 0.9),
    rgba(0, 0, 0, 0.6)
  );
`;

const Img = styled.div`
  height: 128px;
  width: 128px;
  border-radius: 50%;
  border: 1.5px solid #34b8c0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 8px 0;
  padding-top: 16px;
`;

const TextWrapper = styled.div`
  width: 100%;
  padding: 0 1rem;
  text-align: center;
`;

const MoreButton = styled.a`
  font-size: 0.75rem;
  background: #6f51c7;
  text-transform: uppercase;
  padding: 0.5rem 0.25rem;
  margin-top: 1.5rem;
  display: block;
  border-radius: 16px;
  width: 40%;
  text-align: center;
  color: white;
  border: 3px solid #6f51c7;
`;

interface Props {
  img: string;
  title: string;
  text: string;
}

const ServicesItem = (props: any) => {
  return (
    <Wrapper>
      <ImgWrapper>
        <Img className="imgwrap">
          <props.icon
            className="icon"
            width={80}
            height={80}
            fill={"#34b8c0"}
            style={{ transition: "none" }}
          />
        </Img>
      </ImgWrapper>
      <TextWrapper>
        <F4
          style={{
            fontWeight: 600,
            margin: "1.25rem 0",
            color: "#333"
          }}
          className="bigtext"
        >
          {props.title}
        </F4>
        <F6
          style={{
            fontWeight: 400,
            color: "#555",
            height: 84,
            whiteSpace: "normal",
            overflow: "hidden",
            textOverflow: "ellipsis"
          }}
          className="smalltext"
        >
          {props.text}
        </F6>
      </TextWrapper>
      <MoreButton className="morebutton">Devamı</MoreButton>
    </Wrapper>
  );
};

export default ServicesItem;
