import React, { useRef, useEffect } from "react";
import styled from "styled-components";
import { Row, Col } from "grid-with-styled-components";
import { F1, F2, F4, F5, F3 } from "responsive-tools";

import devices from "../devices";

import PhoneIcon from "./PhoneIcon";
import EmailIcon from "./EmailIcon";
import AddressIcon from "./AddressIcon";

const Line = styled.div`
  width: 100%;
  @media ${devices.md}, ${devices.sm}, ${devices.xs} {
    padding: 0 1rem;
  }
`;

const Wrapper = styled.div`
  display: flex;
  width: 1160px;
  max-width: 100%;
  margin: 0 auto;
  padding: 2rem 0;
  flex-direction: column;
`;

const Centered = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  padding-bottom: 1rem;
`;

const ItemWrapper = styled.div`
  display: flex;
  width: 100%;
  padding-bottom: 1rem;
`;

const TextWrapper = styled.div`
  padding: 0 1rem;
`;

const Contact = () => {
  return (
    <Line id="iletisim">
      <Wrapper>
        <Centered>
          <F2 style={{ fontWeight: 300 }}>İletisim</F2>
        </Centered>

        <Row wrap={true}>
          <Col lg={6} md={6} sm={12} xs={12}>
            <ItemWrapper>
              <AddressIcon width={96} height={96} fill="rgb(82, 169, 241)" />
              <TextWrapper>
                <F5
                  style={{
                    fontWeight: 500,
                    textTransform: "uppercase",
                    marginBottom: "0.25rem"
                  }}
                >
                  adres
                </F5>
                <F4 style={{ fontWeight: 300 }}>
                  Necip Fazıl, Yeni Meram Cd. No:142, 42090 Meram/Konya, Türkiye
                </F4>
              </TextWrapper>
            </ItemWrapper>

            <ItemWrapper>
              <PhoneIcon width={64} height={64} fill="rgb(82, 169, 241)" />
              <TextWrapper style={{ paddingLeft: "2rem" }}>
                <F5
                  style={{
                    fontWeight: 500,
                    textTransform: "uppercase",
                    marginBottom: "0.25rem"
                  }}
                >
                  Telefon
                </F5>
                <F4 style={{ fontWeight: 300 }}>033232365235</F4>
              </TextWrapper>
            </ItemWrapper>

            <ItemWrapper>
              <EmailIcon width={64} height={64} fill="rgb(82, 169, 241)" />
              <TextWrapper style={{ paddingLeft: "2rem" }}>
                <F5
                  style={{
                    fontWeight: 500,
                    textTransform: "uppercase",
                    marginBottom: "0.25rem"
                  }}
                >
                  e-mail
                </F5>
                <F3 style={{ fontWeight: 300 }}>info@ahmetdikici.com.tr</F3>
              </TextWrapper>
            </ItemWrapper>
          </Col>
          <Col lg={6} md={6} sm={12} xs={12}>
            <div>
              <img src="/static/address.png" style={{ width: "100%" }} />
            </div>
          </Col>
        </Row>
      </Wrapper>
    </Line>
  );
};

export default Contact;
