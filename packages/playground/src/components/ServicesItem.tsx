import React, { useRef, useEffect } from "react";
import styled from "styled-components";
import { F4, F5 } from "responsive-tools";
import Icon from './ToothIcon';

const Wrapper = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  border: 1px solid white;
  border-radius: 10px;
  padding-bottom: 2rem;
`;

const ImgWrapper = styled.div`
  width: 100%;
  display: flex;
  height: 196px;
  overflow: hidden;
`;

const FancyDiv = styled.div`
  width: 72px;
  display: flex;
  height: 100%;
  background-image: linear-gradient(
    to bottom,
    rgba(91, 140, 204, 0.9),
    rgba(0, 0, 0, 0.6)
  );
  
`;

const Img = styled.img`
  height: 100%;
  width: calc(100% - 72px);
`;

const TextWrapper = styled.div`
  width: 100%;
  padding: 0 1rem;
  text-align: center;
`;

interface Props {
  img: string;
  title: string;
  text: string;
}

const ServicesItem = (props: Props) => {
  return (
    <Wrapper>
      <ImgWrapper>
        <FancyDiv><Icon width={64} fill={'white'}/></FancyDiv>
        <Img src={props.img} />
      </ImgWrapper>
      <TextWrapper>
        <F4
          style={{
            fontWeight: 600,
            textTransform: "uppercase",
            margin: "1.25rem 0"
          }}
        >
          {props.title}
        </F4>
        <F5 style={{ fontWeight: 400, color: 'grey' }}>{props.text}</F5>
      </TextWrapper>
    </Wrapper>
  );
};

export default ServicesItem;
