import React, { useRef, useEffect } from "react";
import styled from "styled-components";
import { F4, F5 } from "responsive-tools";
import { Row, Col } from "grid-with-styled-components";
import { documentToHtmlString } from "@contentful/rich-text-html-renderer";

const Line = styled.div`
  width: 100%;
  margin-top: 9rem;
  margin-bottom: 4rem;
`;

const Wrapper = styled.div`
  display: flex;
  width: 960px;
  max-width: 100%;
  margin: 0 auto;
  padding: 0;
  flex-direction: column;
`;

const Centered = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
`;

const ImgWrapper = styled.div`
  display: flex;
  height: 176px;
  min-width: 200px;
  max-width: 200px;
`;

const Img = styled.img`
  width: 100%;
`;

const TextWrapper = styled.div`
  padding: 0 1rem;
  white-space: normal;
  overflow: hidden;
  text-overflow: ellipsis;
  word-wrap: break-word;
`;

const PostPage = (props: any) => {
  return (
    <Line>
      <Wrapper>
        {/* <F2 style={{ fontWeight: 300, marginBottom: "1rem" }}>Makaleler</F2> */}
        <Row wrap={true}>
          <Col lg={12} md={12} sm={12} xs={12}>
            <div style={{ display: "flex" }}>
              <ImgWrapper>
                <Img src={props.post.image.fields.file.url} />
              </ImgWrapper>
              <TextWrapper>
                <F4
                  style={{
                    fontWeight: 500,
                    textTransform: "uppercase",
                    marginBottom: "1rem"
                  }}
                >
                  {props.post.title}
                </F4>
                <F5
                  style={{ fontWeight: 300 }}
                  dangerouslySetInnerHTML={{
                    __html: documentToHtmlString(props.post.content)
                  }}
                ></F5>
              </TextWrapper>
            </div>
          </Col>
        </Row>
      </Wrapper>
    </Line>
  );
};

export default PostPage;
