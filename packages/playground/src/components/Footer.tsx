import React, { useRef, useEffect } from "react";
import styled from "styled-components";
import { Row, Col } from "grid-with-styled-components";
import { F1, F2, F4, F5, F3 } from "responsive-tools";

const Wrapper = styled.div`
  display: flex;
  width: 100%;
  margin: 0 auto;
  padding: 2rem 0;
  background: #2b2f39;
`;

const Content = styled.div`
  width: 1160px;
  max-width: 100%;
  margin: 0 auto;
`;

const Centered = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  padding-bottom: 2rem;
`;

const BigText = styled(F4)`
  color: white;
  font-weight: 600;
  margin-bottom: 1.5rem;
`;

const SmallText = styled(F5)`
  color: #989ca8;
  text-transform: uppercase;
  padding-bottom: 0.875rem;
  font-weight: 300;
`;

const WorkingHourLine = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Footer = () => {
  return (
    <div>
      <Wrapper style={{ padding: "4rem 0" }}>
        <Content>
          <Row gutter={16} wrap={true}>
            <Col lg={4} md={8} sm={16} xs={16}>
              <div>
                <BigText>Sayfalar</BigText>
                <SmallText>ANA SAYFA</SmallText>
                <SmallText>hakkimizda</SmallText>
                <SmallText>hizmetlerimiz</SmallText>
                <SmallText>iletisim</SmallText>
              </div>
            </Col>
            <Col lg={4} md={8} sm={16} xs={16}>
              <div style={{ width: "80%" }}>
                <BigText>Calisma Saatleri</BigText>
                <WorkingHourLine>
                  <SmallText>hafta ici</SmallText>
                  <SmallText>-</SmallText>
                  <SmallText>09:00 - 19:00</SmallText>
                </WorkingHourLine>
                <WorkingHourLine>
                  <SmallText>cumartesi</SmallText>
                  <SmallText>-</SmallText>
                  <SmallText>09:30 - 19:00</SmallText>
                </WorkingHourLine>
                <WorkingHourLine>
                  <SmallText>pazar</SmallText>
                  <SmallText>-</SmallText>
                  <SmallText>kapali</SmallText>
                </WorkingHourLine>
              </div>
            </Col>
            <Col lg={4} md={8} sm={16} xs={16}>
              <div>
                <BigText>Iletisim</BigText>
                <div style={{ display: "flex" }}>
                  <SmallText style={{ color: "white" }}>telefon</SmallText>
                  <SmallText style={{ padding: "0 1rem" }}>:</SmallText>
                  <SmallText>0 332 324 74 74</SmallText>
                </div>
                <div style={{ display: "flex" }}>
                  <SmallText style={{ color: "white" }}>gsm</SmallText>
                  <SmallText style={{ padding: "0 1rem" }}>:</SmallText>
                  <SmallText>0 544 430 08 11</SmallText>
                </div>
                <div style={{ display: "flex" }}>
                  <SmallText style={{ color: "white" }}>e-mail</SmallText>
                  <SmallText style={{ padding: "0 1rem" }}>:</SmallText>
                  <SmallText>info@ahmetdikici.com.tr</SmallText>
                </div>
                <div style={{ display: "flex", color: "#989ca8" }}>
                  <F3 style={{ marginRight: "0.5rem" }}>
                    <i className="fab fa-facebook-square"></i>
                  </F3>
                  <F3 style={{ marginRight: "0.5rem" }}>
                    <i className="fab fa-instagram"></i>
                  </F3>
                  <F3 style={{ marginRight: "0.5rem" }}>
                    <i className="fab fa-twitter-square"></i>
                  </F3>
                </div>
              </div>
            </Col>
          </Row>
        </Content>
      </Wrapper>
      <Wrapper style={{ background: "#20232c" }}>
        <Content>
          <SmallText style={{ padding: 0 }}>
            COPYRIGHTS © | KONYA DIŞ HEKIMI AHMET DİKİCİ
          </SmallText>
        </Content>
      </Wrapper>
    </div>
  );
};

export default Footer;
