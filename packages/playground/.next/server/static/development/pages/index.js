module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "../../node_modules/@babel/runtime-corejs2/core-js/map.js":
/*!*****************************************************************************!*\
  !*** C:/projects/lernam/node_modules/@babel/runtime-corejs2/core-js/map.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/map */ "core-js/library/fn/map");

/***/ }),

/***/ "../../node_modules/@babel/runtime-corejs2/core-js/object/assign.js":
/*!***************************************************************************************!*\
  !*** C:/projects/lernam/node_modules/@babel/runtime-corejs2/core-js/object/assign.js ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/assign */ "core-js/library/fn/object/assign");

/***/ }),

/***/ "../../node_modules/@babel/runtime-corejs2/core-js/object/define-property.js":
/*!************************************************************************************************!*\
  !*** C:/projects/lernam/node_modules/@babel/runtime-corejs2/core-js/object/define-property.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/define-property */ "core-js/library/fn/object/define-property");

/***/ }),

/***/ "../../node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js":
/*!************************************************************************************************************!*\
  !*** C:/projects/lernam/node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/get-own-property-descriptor */ "core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "../../node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js":
/*!********************************************************************************************!*\
  !*** C:/projects/lernam/node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _defineProperty; });
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/object/define-property */ "../../node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__);

function _defineProperty(obj, key, value) {
  if (key in obj) {
    _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default()(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

/***/ }),

/***/ "../../node_modules/@babel/runtime-corejs2/helpers/esm/extends.js":
/*!*************************************************************************************!*\
  !*** C:/projects/lernam/node_modules/@babel/runtime-corejs2/helpers/esm/extends.js ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _extends; });
/* harmony import */ var _core_js_object_assign__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/object/assign */ "../../node_modules/@babel/runtime-corejs2/core-js/object/assign.js");
/* harmony import */ var _core_js_object_assign__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_assign__WEBPACK_IMPORTED_MODULE_0__);

function _extends() {
  _extends = _core_js_object_assign__WEBPACK_IMPORTED_MODULE_0___default.a || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

/***/ }),

/***/ "../../node_modules/@babel/runtime-corejs2/helpers/extends.js":
/*!*********************************************************************************!*\
  !*** C:/projects/lernam/node_modules/@babel/runtime-corejs2/helpers/extends.js ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Object$assign = __webpack_require__(/*! ../core-js/object/assign */ "../../node_modules/@babel/runtime-corejs2/core-js/object/assign.js");

function _extends() {
  module.exports = _extends = _Object$assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

module.exports = _extends;

/***/ }),

/***/ "../../node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js":
/*!***********************************************************************************************!*\
  !*** C:/projects/lernam/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "../../node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js":
/*!************************************************************************************************!*\
  !*** C:/projects/lernam/node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Object$getOwnPropertyDescriptor = __webpack_require__(/*! ../core-js/object/get-own-property-descriptor */ "../../node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js");

var _Object$defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ "../../node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  } else {
    var newObj = {};

    if (obj != null) {
      for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
          var desc = _Object$defineProperty && _Object$getOwnPropertyDescriptor ? _Object$getOwnPropertyDescriptor(obj, key) : {};

          if (desc.get || desc.set) {
            _Object$defineProperty(newObj, key, desc);
          } else {
            newObj[key] = obj[key];
          }
        }
      }
    }

    newObj["default"] = obj;
    return newObj;
  }
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ "../../node_modules/next/dist/client/link.js":
/*!****************************************************************!*\
  !*** C:/projects/lernam/node_modules/next/dist/client/link.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireWildcard */ "../../node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js");

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "../../node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.default = void 0;

var _map = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/core-js/map */ "../../node_modules/@babel/runtime-corejs2/core-js/map.js"));

var _url = __webpack_require__(/*! url */ "url");

var _react = _interopRequireWildcard(__webpack_require__(/*! react */ "react"));

var _propTypes = _interopRequireDefault(__webpack_require__(/*! prop-types */ "prop-types"));

var _router = _interopRequireDefault(__webpack_require__(/*! ./router */ "../../node_modules/next/dist/client/router.js"));

var _rewriteUrlForExport = __webpack_require__(/*! next-server/dist/lib/router/rewrite-url-for-export */ "next-server/dist/lib/router/rewrite-url-for-export");

var _utils = __webpack_require__(/*! next-server/dist/lib/utils */ "next-server/dist/lib/utils");
/* global __NEXT_DATA__ */


function isLocal(href) {
  const url = (0, _url.parse)(href, false, true);
  const origin = (0, _url.parse)((0, _utils.getLocationOrigin)(), false, true);
  return !url.host || url.protocol === origin.protocol && url.host === origin.host;
}

function memoizedFormatUrl(formatFunc) {
  let lastHref = null;
  let lastAs = null;
  let lastResult = null;
  return (href, as) => {
    if (lastResult && href === lastHref && as === lastAs) {
      return lastResult;
    }

    const result = formatFunc(href, as);
    lastHref = href;
    lastAs = as;
    lastResult = result;
    return result;
  };
}

function formatUrl(url) {
  return url && typeof url === 'object' ? (0, _utils.formatWithValidation)(url) : url;
}

let observer;
const listeners = new _map.default();
const IntersectionObserver = false ? undefined : null;

function getObserver() {
  // Return shared instance of IntersectionObserver if already created
  if (observer) {
    return observer;
  } // Only create shared IntersectionObserver if supported in browser


  if (!IntersectionObserver) {
    return undefined;
  }

  return observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (!listeners.has(entry.target)) {
        return;
      }

      const cb = listeners.get(entry.target);

      if (entry.isIntersecting || entry.intersectionRatio > 0) {
        observer.unobserve(entry.target);
        listeners.delete(entry.target);
        cb();
      }
    });
  }, {
    rootMargin: '200px'
  });
}

const listenToIntersections = (el, cb) => {
  const observer = getObserver();

  if (!observer) {
    return () => {};
  }

  observer.observe(el);
  listeners.set(el, cb);
  return () => {
    observer.unobserve(el);
    listeners.delete(el);
  };
};

class Link extends _react.Component {
  constructor(props) {
    super(props);
    this.p = void 0;

    this.cleanUpListeners = () => {};

    this.formatUrls = memoizedFormatUrl((href, asHref) => {
      return {
        href: formatUrl(href),
        as: asHref ? formatUrl(asHref) : asHref
      };
    });

    this.linkClicked = e => {
      // @ts-ignore target exists on currentTarget
      const {
        nodeName,
        target
      } = e.currentTarget;

      if (nodeName === 'A' && (target && target !== '_self' || e.metaKey || e.ctrlKey || e.shiftKey || e.nativeEvent && e.nativeEvent.which === 2)) {
        // ignore click for new tab / new window behavior
        return;
      }

      let {
        href,
        as
      } = this.formatUrls(this.props.href, this.props.as);

      if (!isLocal(href)) {
        // ignore click if it's outside our scope (e.g. https://google.com)
        return;
      }

      const {
        pathname
      } = window.location;
      href = (0, _url.resolve)(pathname, href);
      as = as ? (0, _url.resolve)(pathname, as) : href;
      e.preventDefault(); //  avoid scroll for urls with anchor refs

      let {
        scroll
      } = this.props;

      if (scroll == null) {
        scroll = as.indexOf('#') < 0;
      } // replace state instead of push if prop is present


      _router.default[this.props.replace ? 'replace' : 'push'](href, as, {
        shallow: this.props.shallow
      }).then(success => {
        if (!success) return;

        if (scroll) {
          window.scrollTo(0, 0);
          document.body.focus();
        }
      });
    };

    if (true) {
      if (props.prefetch) {
        console.warn('Next.js auto-prefetches automatically based on viewport. The prefetch attribute is no longer needed. More: https://err.sh/zeit/next.js/prefetch-true-deprecated');
      }
    }

    this.p = props.prefetch !== false;
  }

  componentWillUnmount() {
    this.cleanUpListeners();
  }

  handleRef(ref) {
    if (this.p && IntersectionObserver && ref && ref.tagName) {
      this.cleanUpListeners();
      this.cleanUpListeners = listenToIntersections(ref, () => {
        this.prefetch();
      });
    }
  } // The function is memoized so that no extra lifecycles are needed
  // as per https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html


  prefetch() {
    if (!this.p || true) return; // Prefetch the JSON page if asked (only in the client)

    const {
      pathname
    } = window.location;
    const {
      href: parsedHref
    } = this.formatUrls(this.props.href, this.props.as);
    const href = (0, _url.resolve)(pathname, parsedHref);

    _router.default.prefetch(href);
  }

  render() {
    let {
      children
    } = this.props;
    const {
      href,
      as
    } = this.formatUrls(this.props.href, this.props.as); // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

    if (typeof children === 'string') {
      children = _react.default.createElement("a", null, children);
    } // This will return the first child, if multiple are provided it will throw an error


    const child = _react.Children.only(children);

    const props = {
      ref: el => {
        this.handleRef(el);

        if (child && typeof child === 'object' && child.ref) {
          if (typeof child.ref === 'function') child.ref(el);else if (typeof child.ref === 'object') {
            child.ref.current = el;
          }
        }
      },
      onMouseEnter: e => {
        if (child.props && typeof child.props.onMouseEnter === 'function') {
          child.props.onMouseEnter(e);
        }

        this.prefetch();
      },
      onClick: e => {
        if (child.props && typeof child.props.onClick === 'function') {
          child.props.onClick(e);
        }

        if (!e.defaultPrevented) {
          this.linkClicked(e);
        }
      } // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
      // defined, we specify the current 'href', so that repetition is not needed by the user

    };

    if (this.props.passHref || child.type === 'a' && !('href' in child.props)) {
      props.href = as || href;
    } // Add the ending slash to the paths. So, we can serve the
    // "<page>/index.html" directly.


    if (false) {}

    return _react.default.cloneElement(child, props);
  }

}

Link.propTypes = void 0;

if (true) {
  const warn = (0, _utils.execOnce)(console.error); // This module gets removed by webpack.IgnorePlugin

  const exact = __webpack_require__(/*! prop-types-exact */ "prop-types-exact");

  Link.propTypes = exact({
    href: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]).isRequired,
    as: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]),
    prefetch: _propTypes.default.bool,
    replace: _propTypes.default.bool,
    shallow: _propTypes.default.bool,
    passHref: _propTypes.default.bool,
    scroll: _propTypes.default.bool,
    children: _propTypes.default.oneOfType([_propTypes.default.element, (props, propName) => {
      const value = props[propName];

      if (typeof value === 'string') {
        warn("Warning: You're using a string directly inside <Link>. This usage has been deprecated. Please add an <a> tag as child of <Link>");
      }

      return null;
    }]).isRequired
  });
}

var _default = Link;
exports.default = _default;

/***/ }),

/***/ "../../node_modules/next/dist/client/router.js":
/*!******************************************************************!*\
  !*** C:/projects/lernam/node_modules/next/dist/client/router.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireWildcard */ "../../node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js");

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "../../node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _extends2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/helpers/extends */ "../../node_modules/@babel/runtime-corejs2/helpers/extends.js"));

var _defineProperty = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "../../node_modules/@babel/runtime-corejs2/core-js/object/define-property.js"));

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _router2 = _interopRequireWildcard(__webpack_require__(/*! next-server/dist/lib/router/router */ "next-server/dist/lib/router/router"));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__(/*! next-server/dist/lib/router-context */ "next-server/dist/lib/router-context");

var _withRouter = _interopRequireDefault(__webpack_require__(/*! ./with-router */ "../../node_modules/next/dist/client/with-router.js"));

exports.withRouter = _withRouter.default;
/* global window */

const singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

const urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components'];
const routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
const coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

(0, _defineProperty.default)(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  (0, _defineProperty.default)(singletonRouter, field, {
    get() {
      const router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = function () {
    const router = getRouter();
    return router[field](...arguments);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, function () {
      const eventField = "on" + event.charAt(0).toUpperCase() + event.substring(1);
      const _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...arguments);
        } catch (err) {
          // tslint:disable-next-line:no-console
          console.error("Error when running the Router event: " + eventField); // tslint:disable-next-line:no-console

          console.error(err.message + "\n" + err.stack);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    const message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


const createRouter = function createRouter() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  const _router = router;
  const instance = {};

  for (const property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = (0, _extends2.default)({}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = function () {
      return _router[field](...arguments);
    };
  });
  return instance;
}

/***/ }),

/***/ "../../node_modules/next/dist/client/with-router.js":
/*!***********************************************************************!*\
  !*** C:/projects/lernam/node_modules/next/dist/client/with-router.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "../../node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.default = withRouter;

var _extends2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/helpers/extends */ "../../node_modules/@babel/runtime-corejs2/helpers/extends.js"));

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _propTypes = _interopRequireDefault(__webpack_require__(/*! prop-types */ "prop-types"));

function withRouter(ComposedComponent) {
  class WithRouteWrapper extends _react.default.Component {
    constructor() {
      super(...arguments);
      this.context = void 0;
    }

    render() {
      return _react.default.createElement(ComposedComponent, (0, _extends2.default)({
        router: this.context.router
      }, this.props));
    }

  }

  WithRouteWrapper.displayName = void 0;
  WithRouteWrapper.getInitialProps = void 0;
  WithRouteWrapper.contextTypes = {
    router: _propTypes.default.object
  };
  WithRouteWrapper.getInitialProps = ComposedComponent.getInitialProps;

  if (true) {
    const name = ComposedComponent.displayName || ComposedComponent.name || 'Unknown';
    WithRouteWrapper.displayName = "withRouter(" + name + ")";
  }

  return WithRouteWrapper;
}

/***/ }),

/***/ "../../node_modules/next/link.js":
/*!****************************************************!*\
  !*** C:/projects/lernam/node_modules/next/link.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/client/link */ "../../node_modules/next/dist/client/link.js")


/***/ }),

/***/ "./pages/index.tsx":
/*!*************************!*\
  !*** ./pages/index.tsx ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/extends */ "../../node_modules/@babel/runtime-corejs2/helpers/esm/extends.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! responsive-tools */ "responsive-tools");
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(responsive_tools__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _src_components_NavBar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../src/components/NavBar */ "./src/components/NavBar.tsx");
/* harmony import */ var _src_components_FullScreenBackground__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../src/components/FullScreenBackground */ "./src/components/FullScreenBackground.tsx");
/* harmony import */ var _src_components_HeroText__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../src/components/HeroText */ "./src/components/HeroText.tsx");
/* harmony import */ var _src_components_Services__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../src/components/Services */ "./src/components/Services.tsx");
/* harmony import */ var _src_components_About__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../src/components/About */ "./src/components/About.tsx");
/* harmony import */ var _src_components_Contact__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../src/components/Contact */ "./src/components/Contact.tsx");
/* harmony import */ var _src_components_Footer__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../src/components/Footer */ "./src/components/Footer.tsx");

var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\pages\\index.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;










const LinkWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.a.withConfig({
  displayName: "pages__LinkWrapper",
  componentId: "sc-1k95it8-0"
})(["padding:0.5rem 2rem;flex-grow:1;flex-shrink:1;flex-basis:0;text-align:center;border:1px solid black;max-width:200px;display:block;"]);
const Logo = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.div.withConfig({
  displayName: "pages__Logo",
  componentId: "sc-1k95it8-1"
})(["display:flex;height:60px;@media ", ",", ",", "{align-self:flex-start;}"], responsive_tools__WEBPACK_IMPORTED_MODULE_3__["devices"].md, responsive_tools__WEBPACK_IMPORTED_MODULE_3__["devices"].sm, responsive_tools__WEBPACK_IMPORTED_MODULE_3__["devices"].xs);
const LogoWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.img.withConfig({
  displayName: "pages__LogoWrapper",
  componentId: "sc-1k95it8-2"
})(["max-height:60px;align-self:flex-start;@media ", "{max-height:48px;}"], responsive_tools__WEBPACK_IMPORTED_MODULE_3__["devices"].xs);

const client = __webpack_require__(/*! contentful */ "contentful").createClient({
  space: "ix4jc2zini6y",
  accessToken: "8ac6bd2e370d19bede49bc705cd6ee874c4a53d453bdff0efb1164428fdc3138"
});

const Index = props => __jsx("div", {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 48
  },
  __self: undefined
}, __jsx(_src_components_NavBar__WEBPACK_IMPORTED_MODULE_4__["default"], {
  links: [{
    text: "Ana Sayfa",
    href: ""
  }, {
    text: "Hizmetlerimiz",
    href: "/#/hizmetler"
  }, {
    text: "Hakkimizda",
    href: "/#/hakkimizda"
  }, {
    text: "Iletisim",
    href: "/#/iletisim"
  }],
  background: "white",
  linkComponent: LinkWrapper,
  height: 80,
  xsHeight: 80,
  withScroll: true,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 49
  },
  __self: undefined
}), __jsx(_src_components_FullScreenBackground__WEBPACK_IMPORTED_MODULE_5__["default"], {
  imageURL: props.bigHeroImage,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 62
  },
  __self: undefined
}), __jsx(_src_components_HeroText__WEBPACK_IMPORTED_MODULE_6__["default"], Object(_babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({}, props.heroProps, {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 63
  },
  __self: undefined
})), __jsx(_src_components_Services__WEBPACK_IMPORTED_MODULE_7__["default"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 64
  },
  __self: undefined
}), __jsx(_src_components_About__WEBPACK_IMPORTED_MODULE_8__["default"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 65
  },
  __self: undefined
}), __jsx(_src_components_Contact__WEBPACK_IMPORTED_MODULE_9__["default"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 66
  },
  __self: undefined
}), __jsx(_src_components_Footer__WEBPACK_IMPORTED_MODULE_10__["default"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 67
  },
  __self: undefined
}));

Index.getInitialProps = async () => {
  const heroProps = {
    header: "i am header",
    bigtext: "i am big text"
  };
  const bigHeroImage = "/static/ahmet_png2.png"; // const types = await client.getContentTypes();
  // const hero = types.items.find((t: any) => t.name === "main-hero");
  // const entries = await client.getEntries({
  //   content_type: hero.sys.id
  // });
  // const heroProps = entries.items[0].fields;
  // const assets = await client.getAssets();
  // // console.log("asset items", assets.items);
  // const asset = await client.getAsset("3r1g5PqZ8g4BVnrE7YEIPC");

  return {
    heroProps,
    bigHeroImage
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Index);

/***/ }),

/***/ "./src/components/About.tsx":
/*!**********************************!*\
  !*** ./src/components/About.tsx ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! responsive-tools */ "responsive-tools");
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(responsive_tools__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var grid_with_styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! grid-with-styled-components */ "grid-with-styled-components");
/* harmony import */ var grid_with_styled_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _devices__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../devices */ "./src/devices.ts");
var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\About.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;





const Line = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "About__Line",
  componentId: "sc-1vr5trk-0"
})(["width:100%;padding:0;background:radial-gradient(ellipse at center,#585858 0%,#232323 100%);color:white;@media ", ",", ",", "{padding:0 1rem;}"], _devices__WEBPACK_IMPORTED_MODULE_4__["default"].md, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].sm, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].xs);
const Wrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "About__Wrapper",
  componentId: "sc-1vr5trk-1"
})(["display:flex;width:1160px;max-width:100%;margin:0 auto;padding:2rem 0;flex-direction:column;"]);
const Centered = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "About__Centered",
  componentId: "sc-1vr5trk-2"
})(["display:flex;justify-content:center;width:100%;padding-bottom:2rem;"]);
const Bg = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "About__Bg",
  componentId: "sc-1vr5trk-3"
})(["background-image:url(\"/static/ahmet_png2.png\");background-repeat:no-repeat;background-size:contain;height:80vh;"]);
const TextWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "About__TextWrapper",
  componentId: "sc-1vr5trk-4"
})([""]);

const About = () => {
  return __jsx(Line, {
    id: "hakkimizda",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: undefined
  }, __jsx(Wrapper, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: undefined
  }, __jsx(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_3__["Row"], {
    gutter: 16,
    wrap: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: undefined
  }, __jsx(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_3__["Col"], {
    lg: 6,
    md: 6,
    sm: 12,
    xs: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: undefined
  }, __jsx(Bg, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: undefined
  })), __jsx(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_3__["Col"], {
    lg: 6,
    md: 6,
    sm: 12,
    xs: 12,
    style: {
      display: "flex",
      alignItems: "center"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: undefined
  }, __jsx(TextWrapper, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: undefined
  }, __jsx(responsive_tools__WEBPACK_IMPORTED_MODULE_2__["F4"], {
    style: {
      fontWeight: 200
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58
    },
    __self: undefined
  }, __jsx("div", {
    style: {
      padding: "4px 0"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    },
    __self: undefined
  }, "Di\u015F Hekimi Ahmet Dikici 2017 y\u0131l\u0131nda Erciyes \xDCniversitesi Di\u015F Hekimli\u011Fi Fak\xFCltesin'den mezun olmu\u015Ftur."), " ", __jsx("div", {
    style: {
      padding: "4px 0"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63
    },
    __self: undefined
  }, "2017 itibari ile Dentova \u0130mplant ve Estetik Akademi Ekibine kat\u0131lm\u0131\u015F,ortaklar\u0131ndan olmu\u015Ftur."), " ", __jsx("div", {
    style: {
      padding: "4px 0"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67
    },
    __self: undefined
  }, "\xC7al\u0131\u015Fmalr\u0131nda daha \xE7ok estetik kozmetik di\u015F hekimli\u011Fi,g\xFCl\xFC\u015F tasar\u0131m\u0131,estetik dolgular,zirkonyum kaplama,kompozit lamina veneer ,porselen lamina veneer ve implantoloji \xFCzerin'de yo\u011Funla\u015Fm\u0131\u015Ft\u0131r. Mesleki yenilikleri ve teknolojiyi yak\u0131ndan takip eden Ahmet D\u0130K\u0130C\u0130 TDB ve EDAD \xFCyesidir. 2018 y\u0131l\u0131n'da Fakt\xF6r\xFCm dergisin'de \"hemofili hastalar\u0131n\u0131n di\u015F tedavileri ve a\u011F\u0131z sa\u011Fl\u0131\u011F\u0131\" konusunda yaz\u0131s\u0131 yay\u0131mlanm\u0131\u015Ft\u0131r."), " ", __jsx("div", {
    style: {
      padding: "4px 0"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76
    },
    __self: undefined
  }, "Seyahat etmeyi,yeni k\xFClt\xFCrler tan\u0131may\u0131 ve d\xFCnyay\u0131 gezmeyi \xE7ok seven Dt. Ahmet D\u0130KC\u0130 \xE7ekti\u011Fi foto\u011Fraflar\u0131 sosyal medya hesab\u0131 \xFCzerinden hastalar\u0131 ve takip\xE7ileri ile payla\u015Fmaktad\u0131r. Ahmet D\u0130K\u0130C\u0130 evlidir ve hali haz\u0131rda Dentova \u0130mplant ve Estetik Akademi'de hastalar\u0131na hizmet vermektedir.")))))));
};

/* harmony default export */ __webpack_exports__["default"] = (About);

/***/ }),

/***/ "./src/components/AddressIcon.tsx":
/*!****************************************!*\
  !*** ./src/components/AddressIcon.tsx ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/extends */ "../../node_modules/@babel/runtime-corejs2/helpers/esm/extends.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);

var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\AddressIcon.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;


const SvgComponent = props => __jsx("svg", Object(_babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
  viewBox: "0 0 100 125"
}, props, {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 4
  },
  __self: undefined
}), __jsx("path", {
  style: {
    textIndent: 0,
    textTransform: "none"
  },
  d: "M68.5 8.978c-11.25 0-20.414 9.064-20.5 20.287l-13.719-4.252a1 1 0 00-.468-.03 1 1 0 00-.094.03l-22 6.815a1 1 0 00-.719.969V90a1 1 0 001.281.938L34 84.217l21.719 6.72a1 1 0 00.562 0l22-6.814a1 1 0 00.719-.938v-33.54a88.772 88.772 0 002.75-3.438C85.732 40.96 88.899 35.32 89 29.452a1 1 0 000-.031c0-11.299-9.196-20.443-20.5-20.443zm0 2c10.224 0 18.5 8.23 18.5 18.443-.09 5.16-2.973 10.468-6.844 15.567-3.63 4.782-8.02 9.336-11.656 13.503-3.638-4.17-8.06-8.715-11.688-13.503C52.95 39.888 50.09 34.582 50 29.42c0-10.215 8.276-18.443 18.5-18.443zm0 9.003c-5.235 0-9.5 4.266-9.5 9.502 0 5.237 4.265 9.503 9.5 9.503S78 34.72 78 29.483c0-5.236-4.265-9.502-9.5-9.502zm0 2c4.154 0 7.5 3.347 7.5 7.502a7.486 7.486 0 01-7.5 7.502 7.486 7.486 0 01-7.5-7.502 7.486 7.486 0 017.5-7.502zM33 27.327v55.108l-20 6.19v-55.11l20-6.188zm2 0l13.156 4.063c.65 5.073 3.434 9.947 6.875 14.535a1 1 0 00-.031.25v42.45l-20-6.19V27.327zm22 21.162c3.577 4.387 7.574 8.455 10.719 12.128a1 1 0 001.531 0c2.299-2.685 5.037-5.605 7.75-8.69v30.508l-20 6.19V48.488z",
  overflow: "visible",
  color: "#000",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 5
  },
  __self: undefined
}));

/* harmony default export */ __webpack_exports__["default"] = (SvgComponent);

/***/ }),

/***/ "./src/components/BookingButton.tsx":
/*!******************************************!*\
  !*** ./src/components/BookingButton.tsx ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\BookingButton.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const BigButton = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.a.withConfig({
  displayName: "BookingButton__BigButton",
  componentId: "sc-149mui1-0"
})(["font-size:1.25rem;background:rgb(9,172,58);text-transform:uppercase;padding:0.75rem 3rem;margin-top:1.5rem;display:inline-block;box-shadow:0 8px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);border-radius:8px;"]);

const BookingButton = () => {
  const buttonEl = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {// const pop1 = (window as any).typeformEmbed.makePopup(
    //   "https://diki.typeform.com/to/ddQvHk",
    //   {
    //     mode: "drawer_right",
    //     hideHeaders: true,
    //     hideFooter: true,
    //     autoClose: true
    //   }
    // );
    // (buttonEl.current as unknown as HTMLElement).addEventListener("click", () => {
    //   pop1.open();
    // });
  });
  return __jsx(BigButton, {
    ref: buttonEl,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: undefined
  }, "Randevu Al");
};

/* harmony default export */ __webpack_exports__["default"] = (BookingButton);

/***/ }),

/***/ "./src/components/Contact.tsx":
/*!************************************!*\
  !*** ./src/components/Contact.tsx ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var grid_with_styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! grid-with-styled-components */ "grid-with-styled-components");
/* harmony import */ var grid_with_styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! responsive-tools */ "responsive-tools");
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(responsive_tools__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _devices__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../devices */ "./src/devices.ts");
/* harmony import */ var _PhoneIcon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./PhoneIcon */ "./src/components/PhoneIcon.tsx");
/* harmony import */ var _EmailIcon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./EmailIcon */ "./src/components/EmailIcon.tsx");
/* harmony import */ var _AddressIcon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./AddressIcon */ "./src/components/AddressIcon.tsx");
var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\Contact.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;








const Line = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Contact__Line",
  componentId: "sc-1buo3oh-0"
})(["width:100%;@media ", ",", ",", "{padding:0 1rem;}"], _devices__WEBPACK_IMPORTED_MODULE_4__["default"].md, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].sm, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].xs);
const Wrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Contact__Wrapper",
  componentId: "sc-1buo3oh-1"
})(["display:flex;width:1160px;max-width:100%;margin:0 auto;padding:2rem 0;flex-direction:column;"]);
const Centered = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Contact__Centered",
  componentId: "sc-1buo3oh-2"
})(["display:flex;justify-content:center;width:100%;padding-bottom:1rem;"]);
const ItemWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Contact__ItemWrapper",
  componentId: "sc-1buo3oh-3"
})(["display:flex;width:100%;padding-bottom:1rem;"]);
const TextWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Contact__TextWrapper",
  componentId: "sc-1buo3oh-4"
})(["padding:0 1rem;"]);

const Contact = () => {
  return __jsx(Line, {
    id: "iletisim",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: undefined
  }, __jsx(Wrapper, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: undefined
  }, __jsx(Centered, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: undefined
  }, __jsx(responsive_tools__WEBPACK_IMPORTED_MODULE_3__["F2"], {
    style: {
      fontWeight: 300
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: undefined
  }, "\u0130letisim")), __jsx(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_2__["Row"], {
    wrap: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: undefined
  }, __jsx(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    lg: 6,
    md: 6,
    sm: 12,
    xs: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54
    },
    __self: undefined
  }, __jsx(ItemWrapper, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55
    },
    __self: undefined
  }, __jsx(_AddressIcon__WEBPACK_IMPORTED_MODULE_7__["default"], {
    width: 96,
    height: 96,
    fill: "rgb(82, 169, 241)",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: undefined
  }), __jsx(TextWrapper, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: undefined
  }, __jsx(responsive_tools__WEBPACK_IMPORTED_MODULE_3__["F5"], {
    style: {
      fontWeight: 500,
      textTransform: "uppercase",
      marginBottom: "0.25rem"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58
    },
    __self: undefined
  }, "adres"), __jsx(responsive_tools__WEBPACK_IMPORTED_MODULE_3__["F4"], {
    style: {
      fontWeight: 300
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67
    },
    __self: undefined
  }, "Necip Faz\u0131l, Yeni Meram Cd. No:142, 42090 Meram/Konya, T\xFCrkiye"))), __jsx(ItemWrapper, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73
    },
    __self: undefined
  }, __jsx(_PhoneIcon__WEBPACK_IMPORTED_MODULE_5__["default"], {
    width: 64,
    height: 64,
    fill: "rgb(82, 169, 241)",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74
    },
    __self: undefined
  }), __jsx(TextWrapper, {
    style: {
      paddingLeft: "2rem"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75
    },
    __self: undefined
  }, __jsx(responsive_tools__WEBPACK_IMPORTED_MODULE_3__["F5"], {
    style: {
      fontWeight: 500,
      textTransform: "uppercase",
      marginBottom: "0.25rem"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76
    },
    __self: undefined
  }, "Telefon"), __jsx(responsive_tools__WEBPACK_IMPORTED_MODULE_3__["F4"], {
    style: {
      fontWeight: 300
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85
    },
    __self: undefined
  }, "033232365235"))), __jsx(ItemWrapper, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 89
    },
    __self: undefined
  }, __jsx(_EmailIcon__WEBPACK_IMPORTED_MODULE_6__["default"], {
    width: 64,
    height: 64,
    fill: "rgb(82, 169, 241)",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 90
    },
    __self: undefined
  }), __jsx(TextWrapper, {
    style: {
      paddingLeft: "2rem"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 91
    },
    __self: undefined
  }, __jsx(responsive_tools__WEBPACK_IMPORTED_MODULE_3__["F5"], {
    style: {
      fontWeight: 500,
      textTransform: "uppercase",
      marginBottom: "0.25rem"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 92
    },
    __self: undefined
  }, "e-mail"), __jsx(responsive_tools__WEBPACK_IMPORTED_MODULE_3__["F3"], {
    style: {
      fontWeight: 300
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 101
    },
    __self: undefined
  }, "info@ahmetdikici.com.tr")))), __jsx(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    lg: 6,
    md: 6,
    sm: 12,
    xs: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 105
    },
    __self: undefined
  }, __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 106
    },
    __self: undefined
  }, __jsx("img", {
    src: "/static/address.png",
    style: {
      width: "100%"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 107
    },
    __self: undefined
  }))))));
};

/* harmony default export */ __webpack_exports__["default"] = (Contact);

/***/ }),

/***/ "./src/components/EmailIcon.tsx":
/*!**************************************!*\
  !*** ./src/components/EmailIcon.tsx ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/extends */ "../../node_modules/@babel/runtime-corejs2/helpers/esm/extends.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);

var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\EmailIcon.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;


const SvgComponent = props => __jsx("svg", Object(_babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
  viewBox: "0 0 100 125"
}, props, {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 4
  },
  __self: undefined
}), __jsx("path", {
  d: "M94.7 15.7H5.3C2.4 15.7 0 18.1 0 21v58c0 2.9 2.4 5.3 5.3 5.3h89.4c2.9 0 5.3-2.4 5.3-5.3V21c0-2.9-2.4-5.3-5.3-5.3zM5.3 18.2h89.4c.3 0 .6.1.8.1L52 60.3c-1.1 1.1-2.9 1.1-4.1 0L4.1 18.5c.3-.2.8-.3 1.2-.3zM2.4 79V21c0-.2 0-.4.1-.7L32.7 49 2.4 79zm92.3 2.8H5.3c-.6 0-1.2-.2-1.7-.5l30.8-30.5L46.3 62c1 1 2.4 1.5 3.7 1.5 1.4 0 2.7-.5 3.7-1.5l11.8-11.4 30.9 30.6c-.4.4-1 .6-1.7.6zm2.9-2.9l-30.3-30 30.1-29c.1.3.2.7.2 1.1v57.9z",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 5
  },
  __self: undefined
}));

/* harmony default export */ __webpack_exports__["default"] = (SvgComponent);

/***/ }),

/***/ "./src/components/Footer.tsx":
/*!***********************************!*\
  !*** ./src/components/Footer.tsx ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var grid_with_styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! grid-with-styled-components */ "grid-with-styled-components");
/* harmony import */ var grid_with_styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! responsive-tools */ "responsive-tools");
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(responsive_tools__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\Footer.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const Wrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Footer__Wrapper",
  componentId: "sc-1mjkbyv-0"
})(["display:flex;width:100%;margin:0 auto;padding:2rem 0;background:#2b2f39;"]);
const Content = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Footer__Content",
  componentId: "sc-1mjkbyv-1"
})(["width:1160px;max-width:100%;margin:0 auto;"]);
const Centered = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Footer__Centered",
  componentId: "sc-1mjkbyv-2"
})(["display:flex;justify-content:center;width:100%;padding-bottom:2rem;"]);
const BigText = styled_components__WEBPACK_IMPORTED_MODULE_1___default()(responsive_tools__WEBPACK_IMPORTED_MODULE_3__["F4"]).withConfig({
  displayName: "Footer__BigText",
  componentId: "sc-1mjkbyv-3"
})(["color:white;font-weight:600;margin-bottom:1.5rem;"]);
const SmallText = styled_components__WEBPACK_IMPORTED_MODULE_1___default()(responsive_tools__WEBPACK_IMPORTED_MODULE_3__["F5"]).withConfig({
  displayName: "Footer__SmallText",
  componentId: "sc-1mjkbyv-4"
})(["color:#989ca8;text-transform:uppercase;padding-bottom:0.875rem;font-weight:300;"]);
const WorkingHourLine = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Footer__WorkingHourLine",
  componentId: "sc-1mjkbyv-5"
})(["display:flex;justify-content:space-between;"]);

const Footer = () => {
  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: undefined
  }, __jsx(Wrapper, {
    style: {
      padding: "4rem 0"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: undefined
  }, __jsx(Content, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: undefined
  }, __jsx(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_2__["Row"], {
    gutter: 16,
    wrap: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: undefined
  }, __jsx(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    lg: 4,
    md: 8,
    sm: 16,
    xs: 16,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51
    },
    __self: undefined
  }, __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: undefined
  }, __jsx(BigText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: undefined
  }, "Sayfalar"), __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54
    },
    __self: undefined
  }, "ANA SAYFA"), __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55
    },
    __self: undefined
  }, "hakkimizda"), __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: undefined
  }, "hizmetlerimiz"), __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: undefined
  }, "iletisim"))), __jsx(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    lg: 4,
    md: 8,
    sm: 16,
    xs: 16,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60
    },
    __self: undefined
  }, __jsx("div", {
    style: {
      width: "80%"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61
    },
    __self: undefined
  }, __jsx(BigText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62
    },
    __self: undefined
  }, "Calisma Saatleri"), __jsx(WorkingHourLine, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63
    },
    __self: undefined
  }, __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64
    },
    __self: undefined
  }, "hafta ici"), __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65
    },
    __self: undefined
  }, "-"), __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66
    },
    __self: undefined
  }, "09:00 - 19:00")), __jsx(WorkingHourLine, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68
    },
    __self: undefined
  }, __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 69
    },
    __self: undefined
  }, "cumartesi"), __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 70
    },
    __self: undefined
  }, "-"), __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71
    },
    __self: undefined
  }, "09:30 - 19:00")), __jsx(WorkingHourLine, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73
    },
    __self: undefined
  }, __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74
    },
    __self: undefined
  }, "pazar"), __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75
    },
    __self: undefined
  }, "-"), __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76
    },
    __self: undefined
  }, "kapali")))), __jsx(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    lg: 4,
    md: 8,
    sm: 16,
    xs: 16,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 80
    },
    __self: undefined
  }, __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81
    },
    __self: undefined
  }, __jsx(BigText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82
    },
    __self: undefined
  }, "Iletisim"), __jsx("div", {
    style: {
      display: "flex"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83
    },
    __self: undefined
  }, __jsx(SmallText, {
    style: {
      color: "white"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84
    },
    __self: undefined
  }, "telefon"), __jsx(SmallText, {
    style: {
      padding: "0 1rem"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85
    },
    __self: undefined
  }, ":"), __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86
    },
    __self: undefined
  }, "0 332 324 74 74")), __jsx("div", {
    style: {
      display: "flex"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 88
    },
    __self: undefined
  }, __jsx(SmallText, {
    style: {
      color: "white"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 89
    },
    __self: undefined
  }, "gsm"), __jsx(SmallText, {
    style: {
      padding: "0 1rem"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 90
    },
    __self: undefined
  }, ":"), __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 91
    },
    __self: undefined
  }, "0 544 430 08 11")), __jsx("div", {
    style: {
      display: "flex"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 93
    },
    __self: undefined
  }, __jsx(SmallText, {
    style: {
      color: "white"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 94
    },
    __self: undefined
  }, "e-mail"), __jsx(SmallText, {
    style: {
      padding: "0 1rem"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 95
    },
    __self: undefined
  }, ":"), __jsx(SmallText, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 96
    },
    __self: undefined
  }, "info@ahmetdikici.com.tr")), __jsx("div", {
    style: {
      display: "flex",
      color: "#989ca8"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 98
    },
    __self: undefined
  }, __jsx(responsive_tools__WEBPACK_IMPORTED_MODULE_3__["F3"], {
    style: {
      marginRight: "0.5rem"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 99
    },
    __self: undefined
  }, __jsx("i", {
    className: "fab fa-facebook-square",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 100
    },
    __self: undefined
  })), __jsx(responsive_tools__WEBPACK_IMPORTED_MODULE_3__["F3"], {
    style: {
      marginRight: "0.5rem"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 102
    },
    __self: undefined
  }, __jsx("i", {
    className: "fab fa-instagram",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 103
    },
    __self: undefined
  })), __jsx(responsive_tools__WEBPACK_IMPORTED_MODULE_3__["F3"], {
    style: {
      marginRight: "0.5rem"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 105
    },
    __self: undefined
  }, __jsx("i", {
    className: "fab fa-twitter-square",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 106
    },
    __self: undefined
  })))))))), __jsx(Wrapper, {
    style: {
      background: "#20232c"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 114
    },
    __self: undefined
  }, __jsx(Content, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 115
    },
    __self: undefined
  }, __jsx(SmallText, {
    style: {
      padding: 0
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 116
    },
    __self: undefined
  }, "COPYRIGHTS \xA9 | KONYA DI\u015E HEKIMI AHMET D\u0130K\u0130C\u0130"))));
};

/* harmony default export */ __webpack_exports__["default"] = (Footer);

/***/ }),

/***/ "./src/components/FullScreenBackground.tsx":
/*!*************************************************!*\
  !*** ./src/components/FullScreenBackground.tsx ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! responsive-tools */ "responsive-tools");
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(responsive_tools__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\FullScreenBackground.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const Bg = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "FullScreenBackground__Bg",
  componentId: "sc-1kccynd-0"
})(["background-image:url(\"", "\");height:100%;width:100%;background-position-x:90%;background-position-y:6rem;background-repeat:no-repeat;background-size:contain;background-attachment:fixed;position:absolute;top:0;left:0;right:0;z-index:-1;&:before{content:\"\";position:absolute;top:0;right:0;bottom:0;left:0;height:100vh;background-image:linear-gradient( to right,rgba(91,140,204,0.9),rgba(158,199,200,0.6) );}"], props => props.imageURL);

const FullScreenBackground = props => {
  return __jsx(Bg, {
    imageURL: props.imageURL,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42
    },
    __self: undefined
  });
};

/* harmony default export */ __webpack_exports__["default"] = (FullScreenBackground);

/***/ }),

/***/ "./src/components/HeroText.tsx":
/*!*************************************!*\
  !*** ./src/components/HeroText.tsx ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _BookingButton__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./BookingButton */ "./src/components/BookingButton.tsx");
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! responsive-tools */ "responsive-tools");
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(responsive_tools__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\HeroText.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const HeroWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "HeroText__HeroWrapper",
  componentId: "sc-11yij0e-0"
})(["height:100vh;width:1160px;margin:0 auto;display:flex;align-items:center;max-width:100%;"]);
const Hero = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "HeroText__Hero",
  componentId: "sc-11yij0e-1"
})(["font-size:4.5rem;width:50%;height:60%;color:rgb(250,250,250);@media ", ",", ",", "{width:100%;padding:0 1rem;}"], responsive_tools__WEBPACK_IMPORTED_MODULE_3__["devices"].md, responsive_tools__WEBPACK_IMPORTED_MODULE_3__["devices"].sm, responsive_tools__WEBPACK_IMPORTED_MODULE_3__["devices"].xs);
const BigFont = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.h1.withConfig({
  displayName: "HeroText__BigFont",
  componentId: "sc-11yij0e-2"
})(["font-size:5rem;margin:0.5rem 0;@media ", "{font-size:4rem;line-height:4.5rem;}"], responsive_tools__WEBPACK_IMPORTED_MODULE_3__["devices"].xs);
const MediumFont = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.p.withConfig({
  displayName: "HeroText__MediumFont",
  componentId: "sc-11yij0e-3"
})(["font-size:2.25rem;margin:0.5rem 0;"]);
const SmallFont = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.p.withConfig({
  displayName: "HeroText__SmallFont",
  componentId: "sc-11yij0e-4"
})(["font-size:1.25rem;margin:0.5rem 0;"]);

const HeroText = props => {
  return __jsx(HeroWrapper, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: undefined
  }, __jsx(Hero, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: undefined
  }, __jsx(MediumFont, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: undefined
  }, props.header), __jsx(BigFont, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: undefined
  }, props.bigtext), __jsx(SmallFont, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51
    },
    __self: undefined
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco", " "), __jsx(_BookingButton__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: undefined
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (HeroText);

/***/ }),

/***/ "./src/components/NavBar.tsx":
/*!***********************************!*\
  !*** ./src/components/NavBar.tsx ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "../../node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/link */ "../../node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _devices__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../devices */ "./src/devices.ts");
/* harmony import */ var _icons_Phone__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./icons/Phone */ "./src/components/icons/Phone.tsx");

var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\NavBar.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;





const BarWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.nav.withConfig({
  displayName: "NavBar__BarWrapper",
  componentId: "zaw3nx-0"
})(["width:100%;display:flex;height:", ";flex-direction:row;transition:height 0.22s;flex-wrap:wrap;background:rgba(0,0,0,0.65);position:fixed;top:0;box-shadow:0px 6px 20px rgba(0,0,0,0.16);@media ", ",", ",", "{height:", ";overflow:hidden;padding:1rem;}"], props => props.height ? props.height + "px" : "auto", _devices__WEBPACK_IMPORTED_MODULE_4__["default"].xs, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].sm, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].md, props => props.xsHeight ? props.xsHeight + "px" : "auto");
const LinksWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.nav.withConfig({
  displayName: "NavBar__LinksWrapper",
  componentId: "zaw3nx-1"
})(["display:flex;margin-left:auto;order:2;@media ", ",", ",", "{order:3;width:100%;flex-direction:column;margin-top:4px;align-items:center;}"], _devices__WEBPACK_IMPORTED_MODULE_4__["default"].xs, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].sm, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].md);
const LinkWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.a.withConfig({
  displayName: "NavBar__LinkWrapper",
  componentId: "zaw3nx-2"
})(["padding:0.5rem 1rem;flex-grow:1;flex-shrink:1;flex-basis:0;width:144px;display:flex;justify-content:center;align-items:center;text-decoration:none;color:white;flex-direction:column;.border{width:0;height:2px;background:white;transition:all 0.32s ease;margin-top:0.5rem;}&.active{.border{width:100%;}}@media ", ",", ",", "{padding:1rem;}"], _devices__WEBPACK_IMPORTED_MODULE_4__["default"].xs, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].sm, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].md);
const TopInfoBar = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.div.withConfig({
  displayName: "NavBar__TopInfoBar",
  componentId: "zaw3nx-3"
})(["display:flex;min-height:", ";width:1160px;max-width:100%;align-items:center;@media ", ",", ",", "{flex-wrap:wrap;}"], props => props.height ? props.height + "px" : "auto", _devices__WEBPACK_IMPORTED_MODULE_4__["default"].xs, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].sm, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].md);
const QuickContactWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.div.withConfig({
  displayName: "NavBar__QuickContactWrapper",
  componentId: "zaw3nx-4"
})(["display:flex;margin-left:auto;align-items:flex-end;padding-top:10px;order:3;@media ", ",", "{order:2;margin-left:auto;padding-top:0;margin-right:4rem;align-items:flex-start;display:block;}@media ", "{display:none;}"], _devices__WEBPACK_IMPORTED_MODULE_4__["default"].md, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].sm, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].xs);
const ContactWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.div.withConfig({
  displayName: "NavBar__ContactWrapper",
  componentId: "zaw3nx-5"
})(["display:flex;color:white;"]);
const IconWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.div.withConfig({
  displayName: "NavBar__IconWrapper",
  componentId: "zaw3nx-6"
})(["width:48px;@media ", "{width:32px;}"], _devices__WEBPACK_IMPORTED_MODULE_4__["default"].xs);
const IconTextWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.div.withConfig({
  displayName: "NavBar__IconTextWrapper",
  componentId: "zaw3nx-7"
})(["font-size:1.25rem;font-weight:300;margin-left:0.5rem;@media ", "{font-size:1rem;}"], _devices__WEBPACK_IMPORTED_MODULE_4__["default"].xs);
const MenuIcon = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.div.withConfig({
  displayName: "NavBar__MenuIcon",
  componentId: "zaw3nx-8"
})(["width:24px;display:none;@media ", ",", ",", "{display:block;position:absolute;right:1rem;top:1rem;}&:after,&:before,& div{background-color:#fff;border-radius:3px;content:\"\";display:block;height:2px;margin:7px 0;transition:all 0.2s ease-in-out;}&:before{transform:", ";}&:after{transform:", ";}& div{transform:", ";}"], _devices__WEBPACK_IMPORTED_MODULE_4__["default"].xs, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].sm, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].md, props => props.isActive ? "translateY(9px) rotate(135deg)" : "initial", props => props.isActive ? "translateY(-9px) rotate(-135deg)" : "initial", props => props.isActive ? "scale(0)" : "initial");
const Logo = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.div.withConfig({
  displayName: "NavBar__Logo",
  componentId: "zaw3nx-9"
})(["display:flex;height:60px;width:169px;@media ", ",", ",", "{align-self:flex-start;}"], _devices__WEBPACK_IMPORTED_MODULE_4__["default"].md, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].sm, _devices__WEBPACK_IMPORTED_MODULE_4__["default"].xs);
const LogoWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.img.withConfig({
  displayName: "NavBar__LogoWrapper",
  componentId: "zaw3nx-10"
})(["max-height:60px;align-self:flex-start;@media ", "{max-height:48px;}"], _devices__WEBPACK_IMPORTED_MODULE_4__["default"].xs);

class NavBar extends react__WEBPACK_IMPORTED_MODULE_1___default.a.Component {
  constructor(props) {
    super(props);

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "handleScroll", void 0);

    const sections = ["hizmetlerimiz", "hakkimizda", "iletisim"];
    this.state = {
      activeEl: "",
      isToggled: false,
      xsHeight: props.xsHeight
    };

    this.handleScroll = () => {
      if (window.scrollY < 300) {
        this.setState({
          activeEl: ""
        });
        return;
      }

      sections.forEach(id => {
        const el = document.getElementById(id);

        if (el === null) {
          return;
        }

        const rect = el.getBoundingClientRect();

        if (rect.top < 80 && rect.top > 0 && this.state.activeEl !== id) {
          this.setState({
            activeEl: id
          });
        }
      });
    };
  }

  componentDidMount() {
    // const sections = ["services", "about", "contact"];
    // const sectionRect = sections.map(id => {
    //   const el = document.getElementById(id);
    //   const rect = (el as HTMLElement).getBoundingClientRect();
    //   return rect;
    // });
    let activeEl = "";

    if (window.location.pathname.indexOf("makaleler") >= 0) {
      activeEl = "makaleler";
    }

    this.setState({
      activeEl
    });

    if (this.props.withScroll) {
      window.addEventListener("scroll", this.handleScroll);
    }
  }

  render() {
    const {
      height = 72,
      background,
      xsHeight = 76
    } = this.props;
    return __jsx("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 248
      },
      __self: this
    }, __jsx(BarWrapper, {
      height: height,
      xsHeight: this.state.xsHeight,
      background: background,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 249
      },
      __self: this
    }, __jsx(TopInfoBar, {
      height: height,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 254
      },
      __self: this
    }, __jsx(Logo, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 255
      },
      __self: this
    }, __jsx(LogoWrapper, {
      src: "/static/logo.png",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 256
      },
      __self: this
    })), __jsx(LinksWrapper, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 258
      },
      __self: this
    }, __jsx(LinkWrapper, {
      className: this.state.activeEl === "" ? "active" : "",
      href: "/",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 259
      },
      __self: this
    }, "Ana Sayfa", __jsx("div", {
      className: "border",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 264
      },
      __self: this
    })), __jsx(LinkWrapper, {
      className: this.state.activeEl === "hizmetlerimiz" ? "active" : "",
      href: "/#hizmetlerimiz",
      onClick: () => this.setState({
        activeEl: "hizmetlerimiz"
      }),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 267
      },
      __self: this
    }, "Hizmetlerimiz", __jsx("div", {
      className: "border",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 275
      },
      __self: this
    })), __jsx(LinkWrapper, {
      className: this.state.activeEl === "hakkimizda" ? "active" : "",
      href: "/#hakkimizda",
      onClick: () => this.setState({
        activeEl: "hakkimizda"
      }),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 278
      },
      __self: this
    }, "Hakk\u0131m\u0131zda", __jsx("div", {
      className: "border",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 284
      },
      __self: this
    })), __jsx(LinkWrapper, {
      className: this.state.activeEl === "iletisim" ? "active" : "",
      href: "/#iletisim",
      onClick: () => this.setState({
        activeEl: "iletisim"
      }),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 287
      },
      __self: this
    }, "\u0130leti\u015Fim", __jsx("div", {
      className: "border",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 293
      },
      __self: this
    })), __jsx(next_link__WEBPACK_IMPORTED_MODULE_3___default.a, {
      href: "/makaleler",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 296
      },
      __self: this
    }, __jsx(LinkWrapper, {
      className: this.state.activeEl === "makaleler" ? "active" : "",
      href: "/#/iletisim",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 297
      },
      __self: this
    }, "Makaleler", __jsx("div", {
      className: "border",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 304
      },
      __self: this
    })))), __jsx(QuickContactWrapper, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 308
      },
      __self: this
    }, __jsx(ContactWrapper, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 309
      },
      __self: this
    }, __jsx(IconWrapper, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 310
      },
      __self: this
    }, __jsx(_icons_Phone__WEBPACK_IMPORTED_MODULE_5__["default"], {
      fill: "rgb(82, 169, 221)",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 311
      },
      __self: this
    })), __jsx(IconTextWrapper, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 313
      },
      __self: this
    }, __jsx("div", {
      style: {
        fontSize: "1rem"
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 314
      },
      __self: this
    }, "Telefon"), "0332 324 24 74")))), __jsx(MenuIcon, {
      isActive: this.state.isToggled,
      onClick: () => {
        this.setState({
          isToggled: !this.state.isToggled
        });

        if (this.state.xsHeight > xsHeight) {
          this.setState({
            xsHeight
          });
        } else {
          this.setState({
            xsHeight: xsHeight * 3.2
          });
        }
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 320
      },
      __self: this
    }, __jsx("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 337
      },
      __self: this
    }))));
  }

} // const NavBar = ({
//   height = 80,
//   logo,
//   links = [],
//   background,
//   xsHeight = 76
// }: Props) => {
//   const [xsHeightState, setHeight] = useState(xsHeight);
//   const [isToggled, setToggled] = useState(false);
//   return (
//     <div>
//       <BarWrapper
//         height={height}
//         xsHeight={xsHeightState}
//         background={background}
//       >
//         <TopInfoBar height={height}>
//           {logo}
//           <LinksWrapper>
//             {links.map(link => (
//               <LinkWrapper className="active" {...link}>{link.text}</LinkWrapper>
//             ))}
//           </LinksWrapper>
//           <QuickContactWrapper>
//             <ContactWrapper>
//               <IconWrapper>
//                 <PhoneIcon fill="rgb(82, 169, 221)" />
//               </IconWrapper>
//               <IconTextWrapper>
//                 <div style={{ fontSize: '1rem' }}>Telefon</div>0332 324 24 74
//               </IconTextWrapper>
//             </ContactWrapper>
//           </QuickContactWrapper>
//         </TopInfoBar>
//         <MenuIcon
//           isActive={isToggled}
//           onClick={() => {
//             setToggled(!isToggled);
//             if (xsHeightState > xsHeight) {
//               setHeight(xsHeight);
//             } else {
//               setHeight(xsHeightState * 4);
//             }
//           }}
//         >
//           <div></div>
//         </MenuIcon>
//       </BarWrapper>
//     </div>
//   );
// };


/* harmony default export */ __webpack_exports__["default"] = (NavBar);

/***/ }),

/***/ "./src/components/PhoneIcon.tsx":
/*!**************************************!*\
  !*** ./src/components/PhoneIcon.tsx ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/extends */ "../../node_modules/@babel/runtime-corejs2/helpers/esm/extends.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);

var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\PhoneIcon.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;


const SvgComponent = props => __jsx("svg", Object(_babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
  "data-name": "Layer 1",
  viewBox: "0 0 100 125"
}, props, {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 4
  },
  __self: undefined
}), __jsx("title", {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 5
  },
  __self: undefined
}, "phone"), __jsx("path", {
  d: "M57.535 14.328a28.169 28.169 0 0128.137 28.137 1.828 1.828 0 003.656 0 31.83 31.83 0 00-31.793-31.793 1.828 1.828 0 000 3.656z",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 6
  },
  __self: undefined
}), __jsx("path", {
  d: "M57.535 27.533a14.95 14.95 0 0114.932 14.932 1.828 1.828 0 103.656 0 18.609 18.609 0 00-18.588-18.588 1.828 1.828 0 000 3.656zM25.477 74.525C36.308 85.341 49.64 92.403 63.02 94.411c10.456 1.568 20.188-.009 28.143-4.56a7.59 7.59 0 003.752-5.744A12.35 12.35 0 0091.045 74L79.876 62.831a12.629 12.629 0 00-9.098-3.932 8.037 8.037 0 00-5.86 2.59l-7.077 7.905a60.426 60.426 0 01-27.235-27.235l7.904-7.076a8.038 8.038 0 002.591-5.861 12.629 12.629 0 00-3.932-9.098L26 8.955a12.35 12.35 0 00-10.107-3.871 7.59 7.59 0 00-5.745 3.753C5.598 16.79 4.021 26.523 5.59 36.979c2.007 13.38 9.07 26.713 19.888 37.546zM13.322 10.654a3.994 3.994 0 013.055-1.945 8.825 8.825 0 017.038 2.832l11.168 11.168a9.101 9.101 0 012.863 6.41 4.42 4.42 0 01-1.375 3.239l-9.924 8.885.56 1.225A63.171 63.171 0 0039.41 60.59a63.171 63.171 0 0018.122 12.702l1.225.561 8.886-9.925a4.418 4.418 0 013.237-1.374 9.101 9.101 0 016.41 2.863l11.17 11.168a8.825 8.825 0 012.831 7.038 3.993 3.993 0 01-1.944 3.055c-7.233 4.137-16.149 5.561-25.784 4.117-12.612-1.893-25.22-8.59-35.5-18.855C17.794 61.657 11.097 49.048 9.204 36.437c-1.444-9.635-.02-18.55 4.117-25.783z",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 7
  },
  __self: undefined
}));

/* harmony default export */ __webpack_exports__["default"] = (SvgComponent);

/***/ }),

/***/ "./src/components/ServiceItem3.tsx":
/*!*****************************************!*\
  !*** ./src/components/ServiceItem3.tsx ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! responsive-tools */ "responsive-tools");
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(responsive_tools__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\ServiceItem3.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const Wrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "ServiceItem3__Wrapper",
  componentId: "sc-19mootd-0"
})(["display:flex;width:100%;flex-direction:column;background:white;padding:2rem;align-items:center;margin:1rem 0;box-shadow:0 0 15px 0 rgba(46,61,73,0.03);border-top-left-radius:50px;border-bottom-right-radius:50px;border-top-right-radius:3px;border-bottom-left-radius:3px;transition:all 0.32s ease;&:hover{*{transition:all 0.32s ease;}background-image:linear-gradient( rgb(52,184,192) 0%,rgb(111,81,199) 100% );.icon{fill:white !important;}.smalltext{color:white !important;}.bigtext{color:white !important;}.imgwrap{border-color:white !important;}.morebutton{border-color:white;}}"]);
const ImgWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "ServiceItem3__ImgWrapper",
  componentId: "sc-19mootd-1"
})(["width:100%;display:flex;height:128px;overflow:hidden;justify-content:center;"]);
const FancyDiv = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "ServiceItem3__FancyDiv",
  componentId: "sc-19mootd-2"
})(["width:72px;display:flex;height:100%;background-image:linear-gradient( to bottom,rgba(91,140,204,0.9),rgba(0,0,0,0.6) );"]);
const Img = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "ServiceItem3__Img",
  componentId: "sc-19mootd-3"
})(["height:128px;width:128px;border-radius:50%;border:1.5px solid #34b8c0;display:flex;flex-direction:column;justify-content:center;align-items:center;padding:8px 0;padding-top:16px;"]);
const TextWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "ServiceItem3__TextWrapper",
  componentId: "sc-19mootd-4"
})(["width:100%;padding:0 1rem;text-align:center;"]);
const MoreButton = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.a.withConfig({
  displayName: "ServiceItem3__MoreButton",
  componentId: "sc-19mootd-5"
})(["font-size:0.75rem;background:#6f51c7;text-transform:uppercase;padding:0.5rem 0.25rem;margin-top:1.5rem;display:block;border-radius:16px;width:40%;text-align:center;color:white;border:3px solid #6f51c7;"]);

const ServicesItem = props => {
  return __jsx(Wrapper, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 107
    },
    __self: undefined
  }, __jsx(ImgWrapper, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 108
    },
    __self: undefined
  }, __jsx(Img, {
    className: "imgwrap",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 109
    },
    __self: undefined
  }, __jsx(props.icon, {
    className: "icon",
    width: 80,
    height: 80,
    fill: "#34b8c0",
    style: {
      transition: "none"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 110
    },
    __self: undefined
  }))), __jsx(TextWrapper, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 119
    },
    __self: undefined
  }, __jsx(responsive_tools__WEBPACK_IMPORTED_MODULE_2__["F4"], {
    style: {
      fontWeight: 600,
      margin: "1.25rem 0",
      color: "#333"
    },
    className: "bigtext",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 120
    },
    __self: undefined
  }, props.title), __jsx(responsive_tools__WEBPACK_IMPORTED_MODULE_2__["F6"], {
    style: {
      fontWeight: 400,
      color: "#555",
      height: 84,
      whiteSpace: "normal",
      overflow: "hidden",
      textOverflow: "ellipsis"
    },
    className: "smalltext",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 130
    },
    __self: undefined
  }, props.text)), __jsx(MoreButton, {
    className: "morebutton",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 144
    },
    __self: undefined
  }, "Devam\u0131"));
};

/* harmony default export */ __webpack_exports__["default"] = (ServicesItem);

/***/ }),

/***/ "./src/components/Services.tsx":
/*!*************************************!*\
  !*** ./src/components/Services.tsx ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! responsive-tools */ "responsive-tools");
/* harmony import */ var responsive_tools__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(responsive_tools__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var grid_with_styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! grid-with-styled-components */ "grid-with-styled-components");
/* harmony import */ var grid_with_styled_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ServiceItem3__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ServiceItem3 */ "./src/components/ServiceItem3.tsx");
/* harmony import */ var _icons_Estetik__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./icons/Estetik */ "./src/components/icons/Estetik.tsx");
/* harmony import */ var _icons_Protez__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./icons/Protez */ "./src/components/icons/Protez.tsx");
var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\Services.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;







const Line = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Services__Line",
  componentId: "sc-1p7gdz4-0"
})(["width:100%;padding:2rem 0;background-image:url(/static/bg-light-about.png);background-size:cover;background-position:center center;background-color:#f5f5f5;"]);
const Wrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Services__Wrapper",
  componentId: "sc-1p7gdz4-1"
})(["display:flex;width:1160px;max-width:100%;margin:0 auto;padding:0;flex-direction:column;"]);
const Centered = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Services__Centered",
  componentId: "sc-1p7gdz4-2"
})(["display:flex;justify-content:center;width:100%;"]);
const Title = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Services__Title",
  componentId: "sc-1p7gdz4-3"
})(["position:relative;&:before{content:\"\";position:absolute;width:120px;height:2px;background-color:#6f51c7;top:55px;right:0;left:0;border-radius:50px;}&:after{content:\"\";position:absolute;width:30px;height:6px;background-color:#ff8700;top:53px;left:0px;right:0;border-radius:50px;}"]);
const serviceItems = [{
  img: "/estetik.jpg",
  title: "Dis Estetigi",
  text: "DİŞ BEYAZLATMA Dişlerin yapısal olarak kendine has bir rengi vardır. Yenilen yemekler, sigara, çay, kahve, kola, şarap, vişne vs gibi",
  icon: _icons_Estetik__WEBPACK_IMPORTED_MODULE_5__["default"]
}, {
  img: "/protez.jpg",
  title: "Dis Protezi",
  text: "Dişlere yapılan protezler harektli ve sabit olmak üzere genellikle ikiye ayrılır. Hareketli protezler takıp çıkarılabilen, sabit protezler is",
  icon: _icons_Protez__WEBPACK_IMPORTED_MODULE_6__["default"]
}, {
  img: "/implant.jpg",
  title: "Implant",
  text: "İmplant İMPLANT UYGULAMALARI Diş implantları, diş eksikliklerinde hastalarımıza fonksiyon,estetik ve sağlığını tekrar geri kazanmaları",
  icon: _icons_Protez__WEBPACK_IMPORTED_MODULE_6__["default"]
}, {
  img: "/ortadonti.jpg",
  title: "Ortadonti",
  text: "ORTODONTİ Diş çapraşıklıkları çocukluk döneminde süt dişlerin yerini asıl dişlerin alması ile başlayan bir süre&cced",
  icon: _icons_Protez__WEBPACK_IMPORTED_MODULE_6__["default"]
}, {
  img: "/kanal_tedavisi.jpg",
  title: "Kanal tedavisi",
  text: "KANAL TEDAVİSİ Her dişin ortasından onun beslenmesini ve duyarlılığını sağlayan damar ve sinir paketi geçer. Bu damar – sinir paketine",
  icon: _icons_Protez__WEBPACK_IMPORTED_MODULE_6__["default"]
}, {
  img: "/lamina.jpg",
  title: "Lamina",
  text: "LAMİNA Porselen Lamina Nedir ? Laminalar özellikle ön grup dişlerde uygulanabilen çok ince porselenlerdir.Porselenin fiziksel özel",
  icon: _icons_Protez__WEBPACK_IMPORTED_MODULE_6__["default"]
}];

const Services = () => {
  return __jsx("div", {
    id: "hizmetlerimiz",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 108
    },
    __self: undefined
  }, __jsx(Line, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 109
    },
    __self: undefined
  }, __jsx(Centered, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 110
    },
    __self: undefined
  }, __jsx(responsive_tools__WEBPACK_IMPORTED_MODULE_2__["F2"], {
    style: {
      fontWeight: 300
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 111
    },
    __self: undefined
  }, "Hizmetlerimiz")), __jsx(Wrapper, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 113
    },
    __self: undefined
  }, __jsx(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_3__["Row"], {
    wrap: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 114
    },
    __self: undefined
  }, serviceItems.map(item => __jsx(grid_with_styled_components__WEBPACK_IMPORTED_MODULE_3__["Col"], {
    lg: 4,
    md: 4,
    sm: 6,
    xs: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 116
    },
    __self: undefined
  }, __jsx("div", {
    style: {
      padding: "0 1rem"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 117
    },
    __self: undefined
  }, __jsx(_ServiceItem3__WEBPACK_IMPORTED_MODULE_4__["default"], {
    img: item.img,
    text: item.text,
    title: item.title,
    icon: item.icon,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 118
    },
    __self: undefined
  }))))))));
};

/* harmony default export */ __webpack_exports__["default"] = (Services);

/***/ }),

/***/ "./src/components/icons/Estetik.tsx":
/*!******************************************!*\
  !*** ./src/components/icons/Estetik.tsx ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/extends */ "../../node_modules/@babel/runtime-corejs2/helpers/esm/extends.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);

var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\icons\\Estetik.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;


const SvgComponent = props => __jsx("svg", Object(_babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
  viewBox: "0 0 64 80"
}, props, {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 4
  },
  __self: undefined
}), __jsx("path", {
  d: "M46.083 42.522a81.008 81.008 0 00-.801 4.58l-1.002 7.014A3.378 3.378 0 0140.953 57a3.365 3.365 0 01-3.301-2.732l-3.073-16.133C34.344 36.897 33.259 36 32 36s-2.344.897-2.579 2.135l-3.073 16.134A3.365 3.365 0 0123.047 57a3.376 3.376 0 01-3.326-2.885l-1.002-7.014a81.486 81.486 0 00-.805-4.578C11.235 46.077 6.217 51.92 3.717 59.107L3.059 61h57.883l-.658-1.893c-2.501-7.188-7.52-13.032-14.201-16.585zM10 3h2v2h-2zM10 7h2v2h-2zM12 5h2v2h-2zM8 5h2v2H8zM5 14h2v2H5zM5 18h2v2H5zM7 16h2v2H7zM3 16h2v2H3zM5 36h2v2H5zM5 40h2v2H5zM7 38h2v2H7zM3 38h2v2H3zM53 34h2v2h-2zM53 38h2v2h-2zM55 36h2v2h-2zM51 36h2v2h-2zM57 23h2v2h-2zM57 27h2v2h-2zM59 25h2v2h-2zM55 25h2v2h-2zM21 30c-2.757 0-5-2.243-5-5h-2c0 2.757-2.243 5-5 5v2c2.757 0 5 2.243 5 5h2c0-2.757 2.243-5 5-5v-2zm-6 3.413A7.045 7.045 0 0012.587 31 7.045 7.045 0 0015 28.587 7.045 7.045 0 0017.413 31 7.054 7.054 0 0015 33.413z",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 5
  },
  __self: undefined
}), __jsx("path", {
  d: "M13 19.764c0 1.118.21 2.201.602 3.236H18v2c0 1.654 1.346 3 3 3h2v6h-2a2.993 2.993 0 00-2.685 1.689A83.814 83.814 0 0120.7 46.818l1.002 7.014A1.365 1.365 0 0023.047 55c.652 0 1.214-.465 1.336-1.105l3.073-16.135C27.872 35.581 29.782 34 32 34s4.128 1.581 4.544 3.76l3.073 16.134A1.362 1.362 0 0040.953 55c.672 0 1.251-.502 1.346-1.168l1.002-7.014a83.57 83.57 0 016.854-23.207A9.108 9.108 0 0051 19.764v-1.583C51 13.118 46.882 9 41.819 9a9.174 9.174 0 00-4.723 1.309l-.868.52c-2.551 1.533-5.905 1.533-8.458 0l-.866-.52A9.174 9.174 0 0022.181 9C17.118 9 13 13.118 13 18.181v1.583zm12.873-7.742l.871.522c3.172 1.904 7.34 1.905 10.516-.003l.829-.496c.686-.41 1.37-.695 2.036-.85a7.61 7.61 0 011.904-.193l-.059 2c-.463-.02-.93.034-1.394.142-.463.107-.955.315-1.462.617l-.827.495c-1.61.968-3.423 1.522-5.288 1.675V28h-2V15.932c-1.864-.154-3.676-.706-5.285-1.672l-.871-.522a5.137 5.137 0 00-2.814-.734l-.059-2a7.197 7.197 0 013.903 1.018zM56 3h-2c0 2.757-2.243 5-5 5v2c2.757 0 5 2.243 5 5h2c0-2.757 2.243-5 5-5V8c-2.757 0-5-2.243-5-5zm-1 8.413A7.045 7.045 0 0052.587 9 7.045 7.045 0 0055 6.587 7.045 7.045 0 0057.413 9 7.054 7.054 0 0055 11.413z",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 6
  },
  __self: undefined
}));

/* harmony default export */ __webpack_exports__["default"] = (SvgComponent);

/***/ }),

/***/ "./src/components/icons/Phone.tsx":
/*!****************************************!*\
  !*** ./src/components/icons/Phone.tsx ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/extends */ "../../node_modules/@babel/runtime-corejs2/helpers/esm/extends.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);

var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\icons\\Phone.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;


const SvgComponent = props => __jsx("svg", Object(_babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
  "data-name": "Layer 1",
  viewBox: "0 0 100 125"
}, props, {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 4
  },
  __self: undefined
}), __jsx("title", {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 5
  },
  __self: undefined
}, "phone"), __jsx("path", {
  d: "M57.535 14.328a28.169 28.169 0 0128.137 28.137 1.828 1.828 0 003.656 0 31.83 31.83 0 00-31.793-31.793 1.828 1.828 0 000 3.656z",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 6
  },
  __self: undefined
}), __jsx("path", {
  d: "M57.535 27.533a14.95 14.95 0 0114.932 14.932 1.828 1.828 0 103.656 0 18.609 18.609 0 00-18.588-18.588 1.828 1.828 0 000 3.656zM25.477 74.525C36.308 85.341 49.64 92.403 63.02 94.411c10.456 1.568 20.188-.009 28.143-4.56a7.59 7.59 0 003.752-5.744A12.35 12.35 0 0091.045 74L79.876 62.831a12.629 12.629 0 00-9.098-3.932 8.037 8.037 0 00-5.86 2.59l-7.077 7.905a60.426 60.426 0 01-27.235-27.235l7.904-7.076a8.038 8.038 0 002.591-5.861 12.629 12.629 0 00-3.932-9.098L26 8.955a12.35 12.35 0 00-10.107-3.871 7.59 7.59 0 00-5.745 3.753C5.598 16.79 4.021 26.523 5.59 36.979c2.007 13.38 9.07 26.713 19.888 37.546zM13.322 10.654a3.994 3.994 0 013.055-1.945 8.825 8.825 0 017.038 2.832l11.168 11.168a9.101 9.101 0 012.863 6.41 4.42 4.42 0 01-1.375 3.239l-9.924 8.885.56 1.225A63.171 63.171 0 0039.41 60.59a63.171 63.171 0 0018.122 12.702l1.225.561 8.886-9.925a4.418 4.418 0 013.237-1.374 9.101 9.101 0 016.41 2.863l11.17 11.168a8.825 8.825 0 012.831 7.038 3.993 3.993 0 01-1.944 3.055c-7.233 4.137-16.149 5.561-25.784 4.117-12.612-1.893-25.22-8.59-35.5-18.855C17.794 61.657 11.097 49.048 9.204 36.437c-1.444-9.635-.02-18.55 4.117-25.783z",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 7
  },
  __self: undefined
}));

/* harmony default export */ __webpack_exports__["default"] = (SvgComponent);

/***/ }),

/***/ "./src/components/icons/Protez.tsx":
/*!*****************************************!*\
  !*** ./src/components/icons/Protez.tsx ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/extends */ "../../node_modules/@babel/runtime-corejs2/helpers/esm/extends.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);

var _jsxFileName = "C:\\projects\\lernam\\packages\\playground\\src\\components\\icons\\Protez.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;


const SvgComponent = props => __jsx("svg", Object(_babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
  viewBox: "0 0 64 80"
}, props, {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 4
  },
  __self: undefined
}), __jsx("path", {
  d: "M9 17c.988 0 1.648-.29 2-.502V13H7v3.5c.35.209 1.014.5 2 .5zM22 27.502v2.99c1.309.267 2.647.42 4 .475V27.5c-.35-.209-1.014-.5-2-.5-.988 0-1.648.29-2 .502zM15 17c.988 0 1.648-.29 2-.502V13h-4v3.5c.35.209 1.014.5 2 .5z",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 5
  },
  __self: undefined
}), __jsx("path", {
  d: "M14 27.362v-.776l.293-.293C14.425 26.161 15.653 25 18 25c1.399 0 2.391.412 3 .773.609-.361 1.601-.773 3-.773s2.391.412 3 .773c.609-.361 1.601-.773 3-.773.427 0 .803.048 1.156.112A15.99 15.99 0 0131 23c0-1.441.209-2.832.568-4.161A5.425 5.425 0 0130 18.228c-.609.36-1.601.772-3 .772s-2.391-.412-3-.773c-.609.361-1.601.773-3 .773s-2.391-.412-3-.773c-.609.361-1.601.773-3 .773s-2.391-.412-3-.773c-.609.361-1.601.773-3 .773-2.347 0-3.575-1.161-3.707-1.293L5 17.414V13H3.52c-.069.239-.142.476-.199.721l-.257 1.115 2.193 3.728A25.193 25.193 0 0014 27.362z",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 6
  },
  __self: undefined
}), __jsx("path", {
  d: "M21 17c.988 0 1.648-.29 2-.502V13h-4v3.5c.35.209 1.014.5 2 .5zM31 13v3.5c.244.146.652.327 1.205.426A16.017 16.017 0 0134.53 13H31zM47 9c-7.72 0-14 6.28-14 14s6.28 14 14 14 14-6.28 14-14S54.72 9 47 9zm9.213 21.686l-1.535-1.282A10.014 10.014 0 0057 23c0-3.251-1.566-6.136-3.976-7.963l-1.26 4.831c-1.274 4.884-4.018 9.318-7.779 12.665A9.951 9.951 0 0047 33c2.336 0 4.61-.825 6.403-2.322l1.281 1.535A12.006 12.006 0 0147 35c-6.617 0-12-5.383-12-12s5.383-12 12-12 12 5.383 12 12c0 2.804-.99 5.533-2.787 7.686zM30 27c-.988 0-1.648.29-2 .502v3.463a25.222 25.222 0 004.787-.65 15.927 15.927 0 01-1.182-3.011A4.112 4.112 0 0030 27zM27 17c.988 0 1.648-.29 2-.502V13h-4v3.5c.35.209 1.014.5 2 .5zM46.901 7.005C44.334 4.477 40.857 3 37.202 3a13.877 13.877 0 00-8.64 3.031L27 7.281l-1.562-1.25A13.877 13.877 0 0016.798 3a13.75 13.75 0 00-12.535 8h32.189c2.797-2.461 6.446-3.97 10.449-3.995z",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 7
  },
  __self: undefined
}), __jsx("path", {
  d: "M47 16.5V13a9.93 9.93 0 00-4 .841v2.66c.35.209 1.014.5 2 .5.983-.001 1.646-.29 2-.501zM16 27.5v.956a25.308 25.308 0 004 1.532V27.5c-.35-.209-1.014-.5-2-.5-.983 0-1.646.289-2 .5zM47 39c-.338 0-.668-.03-1-.051V41h2v-2.051c-.332.021-.662.051-1 .051z",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 8
  },
  __self: undefined
}), __jsx("path", {
  d: "M50.676 13.709A9.963 9.963 0 0049 13.201v4.213l-.293.293C48.575 17.839 47.347 19 45 19s-3.575-1.161-3.707-1.293L41 17.414v-2.395c-2.423 1.827-4 4.719-4 7.981 0 1.912.549 3.694 1.484 5.215a25.106 25.106 0 0010.259-9.651l2.193-3.728-.257-1.116-.003-.011zM39.715 29.829a10.024 10.024 0 002.249 1.796 23.32 23.32 0 007.118-9.944 27.075 27.075 0 01-9.367 8.148zM44 43h6v2h-6zM33.861 32.106C31.621 32.689 29.32 33 27 33c-8.763 0-17.004-4.289-22.082-11.326C8.332 30.804 17.087 37 27 37c3.253 0 6.495-.686 9.468-1.986a16.128 16.128 0 01-2.607-2.908zM44 58c0 1.654 1.346 3 3 3s3-1.346 3-3V47h-6v11z",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 9
  },
  __self: undefined
}));

/* harmony default export */ __webpack_exports__["default"] = (SvgComponent);

/***/ }),

/***/ "./src/devices.ts":
/*!************************!*\
  !*** ./src/devices.ts ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const size = {
  xs: "320px",
  sm: "768px",
  md: "992px",
  lg: "1200px"
};
/* harmony default export */ __webpack_exports__["default"] = ({
  xxs: `(max-width: ${size.xs})`,
  xs: `(min-width: ${size.xs}) and (max-width: ${size.sm})`,
  sm: `(min-width: ${size.sm}) and (max-width: ${size.md})`,
  md: `(min-width: ${size.md}) and (max-width: ${size.lg})`,
  lg: `(min-width: ${size.lg})`
});

/***/ }),

/***/ 4:
/*!*******************************!*\
  !*** multi ./pages/index.tsx ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\projects\lernam\packages\playground\pages\index.tsx */"./pages/index.tsx");


/***/ }),

/***/ "contentful":
/*!*****************************!*\
  !*** external "contentful" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("contentful");

/***/ }),

/***/ "core-js/library/fn/map":
/*!*****************************************!*\
  !*** external "core-js/library/fn/map" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/map");

/***/ }),

/***/ "core-js/library/fn/object/assign":
/*!***************************************************!*\
  !*** external "core-js/library/fn/object/assign" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/assign");

/***/ }),

/***/ "core-js/library/fn/object/define-property":
/*!************************************************************!*\
  !*** external "core-js/library/fn/object/define-property" ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-property");

/***/ }),

/***/ "core-js/library/fn/object/get-own-property-descriptor":
/*!************************************************************************!*\
  !*** external "core-js/library/fn/object/get-own-property-descriptor" ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "grid-with-styled-components":
/*!**********************************************!*\
  !*** external "grid-with-styled-components" ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("grid-with-styled-components");

/***/ }),

/***/ "next-server/dist/lib/router-context":
/*!******************************************************!*\
  !*** external "next-server/dist/lib/router-context" ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-server/dist/lib/router-context");

/***/ }),

/***/ "next-server/dist/lib/router/rewrite-url-for-export":
/*!*********************************************************************!*\
  !*** external "next-server/dist/lib/router/rewrite-url-for-export" ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-server/dist/lib/router/rewrite-url-for-export");

/***/ }),

/***/ "next-server/dist/lib/router/router":
/*!*****************************************************!*\
  !*** external "next-server/dist/lib/router/router" ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-server/dist/lib/router/router");

/***/ }),

/***/ "next-server/dist/lib/utils":
/*!*********************************************!*\
  !*** external "next-server/dist/lib/utils" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-server/dist/lib/utils");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "prop-types-exact":
/*!***********************************!*\
  !*** external "prop-types-exact" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types-exact");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "responsive-tools":
/*!***********************************!*\
  !*** external "responsive-tools" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("responsive-tools");

/***/ }),

/***/ "styled-components":
/*!************************************!*\
  !*** external "styled-components" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-components");

/***/ }),

/***/ "url":
/*!**********************!*\
  !*** external "url" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("url");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map