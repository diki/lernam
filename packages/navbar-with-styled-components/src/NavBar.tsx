import React, { useState, FunctionComponent } from "react";
import styled from "styled-components";
import devices from "./devices";
import PhoneIcon from "./PhoneIcon";
import { HamburgerArrow } from "react-animated-burgers";

interface Props {
  height?: number;
  xsHeight?: number;
  logo?: JSX.Element | string;
  links?: Array<{ text: string; href: string }>;
  background?: string;
  linkComponent: React.ComponentClass<any> | FunctionComponent<any>;
}

const BarWrapper = styled.nav<Partial<Props>>`
  width: 100%;
  display: flex;
  height: ${props => (props.height ? props.height + "px" : "auto")};
  // background: ${props => (props.background ? props.background : "")};
  flex-direction: row;
  transition: height 0.22s;
  flex-wrap: wrap;
  background: rgba(0, 0, 0, 0.65);
  position: fixed;
  @media ${devices.xs} {
    height: ${props => (props.xsHeight ? props.xsHeight + "px" : "auto")};
    overflow: hidden;
    padding: 0 1rem;
  }
`;

const LinksWrapper = styled.nav`
  display: flex;
  margin-top: -16px;
  margin-left: auto;
  order: 2;
  @media ${devices.xs} {
    order: 3;
    width: 100%;
    flex-direction: column;
    margin-top: 4px;
    align-items: center;
  }
`;

const LinkWrapper = styled.a`
  padding: 0.5rem 2rem;
  flex-grow: 1;
  flex-shrink: 1;
  flex-basis: 0;
  width: 160px;
  display: flex;
  justify-content: center;
  align-items: center;
  text-decoration: none;
  color: white;
`;

const TopInfoBar = styled.div<{ height: number }>`
  display: flex;
  padding-top: 1rem;
  padding-bottom: 4px;
  min-height: ${props => (props.height ? props.height + "px" : "auto")};
  width: 1160px;
  max-width: 100%;
  margin: 0 auto;
  @media ${devices.xs} {
    flex-wrap: wrap;
    // flex-direction: column;
  }
`;

const QuickContactWrapper = styled.div`
  display: flex;
  margin-left: auto;
  align-items: flex-end;
  padding-top: 10px;
  order: 3;
  @media ${devices.xs} {
    order: 2;
    margin-left: 2rem;
    padding-top: 0;
    align-items: flex-start;
  }
`;

const ContactWrapper = styled.div`
  display: flex;
  color: white;
`;

const IconWrapper = styled.div`
  width: 48px;
  @media ${devices.xs} {
    width: 32px;
  }
`;

const IconTextWrapper = styled.div`
  font-size: 1.25rem;
  font-weight: 300;
  margin-left: 0.5rem;
  @media ${devices.xs} {
    font-size: 1rem;
  }
`;

const MenuIcon = styled.div<{ isActive: boolean }>`
  width: 24px;
  display: none;

  @media ${devices.xs} {
    display: block;
    position: absolute;
    right: 1rem;
    top: 1rem;
  }

  &:after,
  &:before,
  & div {
    background-color: #fff;
    border-radius: 3px;
    content: "";
    display: block;
    height: 2px;
    margin: 7px 0;
    transition: all 0.2s ease-in-out;
  }

  &:before {
    transform: ${props =>
      props.isActive ? "translateY(9px) rotate(135deg)" : "initial"};
  }

  &:after {
    transform: ${props =>
      props.isActive ? "translateY(-9px) rotate(-135deg)" : "initial"};
  }

  & div {
    transform: ${props => (props.isActive ? "scale(0)" : "initial")};
  }
`;

const NavBar = ({
  height = 80,
  logo,
  links = [],
  background,
  xsHeight = 76
}: Props) => {
  const [xsHeightState, setHeight] = useState(xsHeight);
  const [isToggled, setToggled] = useState(false);

  return (
    <div>
      <BarWrapper
        height={height}
        xsHeight={xsHeightState}
        background={background}
      >
        <TopInfoBar height={height}>
          {logo}
          <LinksWrapper>
            {links.map(link => (
              <LinkWrapper {...link}>{link.text}</LinkWrapper>
            ))}
          </LinksWrapper>
          <QuickContactWrapper>
            <ContactWrapper>
              <IconWrapper>
                <PhoneIcon fill="rgb(82, 169, 221)" />
              </IconWrapper>
              <IconTextWrapper>
                <div>Telefon</div>0332 324 24 74
              </IconTextWrapper>
            </ContactWrapper>
          </QuickContactWrapper>
        </TopInfoBar>

        <MenuIcon
          isActive={isToggled}
          onClick={() => {
            setToggled(!isToggled);
            if (xsHeightState > xsHeight) {
              setHeight(xsHeight);
            } else {
              setHeight(xsHeightState * 4);
            }
          }}
        >
          <div></div>
        </MenuIcon>
      </BarWrapper>
    </div>
  );
};

export default NavBar;
