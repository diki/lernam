"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const SvgComponent = (props) => (react_1.default.createElement("svg", Object.assign({ viewBox: "0 0 100 125" }, props),
    react_1.default.createElement("path", { d: "M94.7 15.7H5.3C2.4 15.7 0 18.1 0 21v58c0 2.9 2.4 5.3 5.3 5.3h89.4c2.9 0 5.3-2.4 5.3-5.3V21c0-2.9-2.4-5.3-5.3-5.3zM5.3 18.2h89.4c.3 0 .6.1.8.1L52 60.3c-1.1 1.1-2.9 1.1-4.1 0L4.1 18.5c.3-.2.8-.3 1.2-.3zM2.4 79V21c0-.2 0-.4.1-.7L32.7 49 2.4 79zm92.3 2.8H5.3c-.6 0-1.2-.2-1.7-.5l30.8-30.5L46.3 62c1 1 2.4 1.5 3.7 1.5 1.4 0 2.7-.5 3.7-1.5l11.8-11.4 30.9 30.6c-.4.4-1 .6-1.7.6zm2.9-2.9l-30.3-30 30.1-29c.1.3.2.7.2 1.1v57.9z" })));
exports.default = SvgComponent;
