import React, { FunctionComponent } from "react";
interface Props {
    height?: number;
    xsHeight?: number;
    logo?: JSX.Element | string;
    links?: Array<{
        text: string;
        href: string;
    }>;
    background?: string;
    linkComponent: React.ComponentClass<any> | FunctionComponent<any>;
}
declare const NavBar: ({ height, logo, links, background, xsHeight }: Props) => JSX.Element;
export default NavBar;
//# sourceMappingURL=NavBar.d.ts.map