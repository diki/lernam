"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const styled_components_1 = __importDefault(require("styled-components"));
const devices_1 = __importDefault(require("./devices"));
const PhoneIcon_1 = __importDefault(require("./PhoneIcon"));
const BarWrapper = styled_components_1.default.nav `
  width: 100%;
  display: flex;
  height: ${props => (props.height ? props.height + "px" : "auto")};
  // background: ${props => (props.background ? props.background : "")};
  flex-direction: row;
  transition: height 0.22s;
  flex-wrap: wrap;
  background: rgba(0, 0, 0, 0.65);
  position: fixed;
  @media ${devices_1.default.xs} {
    height: ${props => (props.xsHeight ? props.xsHeight + "px" : "auto")};
    overflow: hidden;
    padding: 0 1rem;
  }
`;
const LinksWrapper = styled_components_1.default.nav `
  display: flex;
  margin-top: -16px;
  margin-left: auto;
  order: 2;
  @media ${devices_1.default.xs} {
    order: 3;
    width: 100%;
    flex-direction: column;
    margin-top: 4px;
    align-items: center;
  }
`;
const LinkWrapper = styled_components_1.default.a `
  padding: 0.5rem 2rem;
  flex-grow: 1;
  flex-shrink: 1;
  flex-basis: 0;
  width: 160px;
  display: flex;
  justify-content: center;
  align-items: center;
  text-decoration: none;
  color: white;
`;
const TopInfoBar = styled_components_1.default.div `
  display: flex;
  padding-top: 1rem;
  padding-bottom: 4px;
  min-height: ${props => (props.height ? props.height + "px" : "auto")};
  width: 1160px;
  max-width: 100%;
  margin: 0 auto;
  @media ${devices_1.default.xs} {
    flex-wrap: wrap;
    // flex-direction: column;
  }
`;
const QuickContactWrapper = styled_components_1.default.div `
  display: flex;
  margin-left: auto;
  align-items: flex-end;
  padding-top: 10px;
  order: 3;
  @media ${devices_1.default.xs} {
    order: 2;
    margin-left: 2rem;
    padding-top: 0;
    align-items: flex-start;
  }
`;
const ContactWrapper = styled_components_1.default.div `
  display: flex;
  color: white;
`;
const IconWrapper = styled_components_1.default.div `
  width: 48px;
  @media ${devices_1.default.xs} {
    width: 32px;
  }
`;
const IconTextWrapper = styled_components_1.default.div `
  font-size: 1.25rem;
  font-weight: 300;
  margin-left: 0.5rem;
  @media ${devices_1.default.xs} {
    font-size: 1rem;
  }
`;
const MenuIcon = styled_components_1.default.div `
  width: 24px;
  display: none;

  @media ${devices_1.default.xs} {
    display: block;
    position: absolute;
    right: 1rem;
    top: 1rem;
  }

  &:after,
  &:before,
  & div {
    background-color: #fff;
    border-radius: 3px;
    content: "";
    display: block;
    height: 2px;
    margin: 7px 0;
    transition: all 0.2s ease-in-out;
  }

  &:before {
    transform: ${props => props.isActive ? "translateY(9px) rotate(135deg)" : "initial"};
  }

  &:after {
    transform: ${props => props.isActive ? "translateY(-9px) rotate(-135deg)" : "initial"};
  }

  & div {
    transform: ${props => (props.isActive ? "scale(0)" : "initial")};
  }
`;
const NavBar = ({ height = 80, logo, links = [], background, xsHeight = 76 }) => {
    const [xsHeightState, setHeight] = react_1.useState(xsHeight);
    const [isToggled, setToggled] = react_1.useState(false);
    return (react_1.default.createElement("div", null,
        react_1.default.createElement(BarWrapper, { height: height, xsHeight: xsHeightState, background: background },
            react_1.default.createElement(TopInfoBar, { height: height },
                logo,
                react_1.default.createElement(LinksWrapper, null, links.map(link => (react_1.default.createElement(LinkWrapper, Object.assign({}, link), link.text)))),
                react_1.default.createElement(QuickContactWrapper, null,
                    react_1.default.createElement(ContactWrapper, null,
                        react_1.default.createElement(IconWrapper, null,
                            react_1.default.createElement(PhoneIcon_1.default, { fill: "rgb(82, 169, 221)" })),
                        react_1.default.createElement(IconTextWrapper, null,
                            react_1.default.createElement("div", null, "Telefon"),
                            "0332 324 24 74")))),
            react_1.default.createElement(MenuIcon, { isActive: isToggled, onClick: () => {
                    setToggled(!isToggled);
                    if (xsHeightState > xsHeight) {
                        setHeight(xsHeight);
                    }
                    else {
                        setHeight(xsHeightState * 4);
                    }
                } },
                react_1.default.createElement("div", null)))));
};
exports.default = NavBar;
